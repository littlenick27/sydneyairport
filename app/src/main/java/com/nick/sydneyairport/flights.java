/**
 * The flights utilises the Avatiaton Edge API (https://aviation-edge.com/) to get flight data
 */



package com.nick.sydneyairport;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class flights extends AppCompatActivity {

    private static final String TAG = "flights";
    private TextView mTextMessage;
    private TextView flightResult;
    private String flightNO;
    private int AD;
    private RequestQueue mRequestQueue;
    private String url = "";
    Button refresh;
    private DatabaseReference mDatabaseReference;
    private ValueEventListener mDatabaseListener;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("key", flightNO);
                    intent.putExtra("key2",AD);
                    startActivity(intent);
                    return true;
                case R.id.navigation_flights:
                    //mTextMessage.setText(R.string.title_flights);
                    Intent intent2 = new Intent(getApplicationContext(), flights.class);
                    intent2.putExtra("key", flightNO);
                    intent2.putExtra("key2",AD);
                    startActivity(intent2);
                    return true;
                case R.id.navigation_map:
                    //mTextMessage.setText(R.string.title_map);
                    Intent intent4 = new Intent(getApplicationContext(), mapsActivity.class);
                    startActivity(intent4);
                    return true;
                case R.id.navigation_stores:
                    //mTextMessage.setText(R.string.title_stores);
                    Intent intent5 = new Intent(getApplicationContext(), locations.class);
                    startActivity(intent5);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flights);
        flightNO = getIntent().getStringExtra("key");
        AD = getIntent().getIntExtra("key2", 3);
        flightResult = (TextView) findViewById(R.id.flightText);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        refresh = findViewById(R.id.button5);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFromDatabase();
            }
        });
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try{
                    mDatabaseReference.removeValue();
                    getFlights();

                }
                catch (Exception e) {
                }
                finally{
                    //also call the same runnable to call it at regular interval
                    handler.postDelayed(this, 60000);
                }
            }
        };
        handler.postDelayed(runnable, 0);
        getFromDatabase();
    }

    //gets flight from database
    private void getFromDatabase(){
        if (flightNO == null) {
            flightResult.append("Please go back to the home page and type in a flight");
        } else {
            mDatabaseReference.child(flightNO).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        flightData flightData = dataSnapshot.getValue(flightData.class);
                        flightResult.setText("");
                        flightResult.append("\n" + "Flight Number: " + flightData.flight +
                                "\n" + "Gate: " + flightData.gate +
                                "\n" + "Time: " + flightData.properEstimatedTime);

                        //gets flight from API
                    } else {
                        if (AD == 0) {
                            url = "http://aviation-edge.com/v2/public/timetable?key=[insertAPIkey]&iataCode=SYD&type=departure";
                        } else if (AD == 1) {
                            url = "http://aviation-edge.com/v2/public/timetable?key=[insertAPIkey]&iataCode=SYD&type=arrival";
                        } else {
                            //do nothing
                        }
                        getFlights();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w(TAG, "loadFlight:onCancelled", databaseError.toException());
                }
            });

        }
    }

    //gets flight from API
    private void getFlights() {

        //RequestQueue initialized
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue = Volley.newRequestQueue(this);

        //String Request initialized

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    flightResult.setText("");

                    int foundFlight = 0;
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject checkFlightNO = response.getJSONObject(i).optJSONObject("flight");
                        String flight = checkFlightNO.getString("iataNumber");
                        if (flight.equals(flightNO)) {
                            foundFlight = 1;
                            if (AD == 0) {
                                JSONObject getInfo = response.getJSONObject(i).optJSONObject("departure");
                                String gate = getInfo.getString("gate");
                                String estimatedTime = getInfo.getString("estimatedTime");
                                String properEstimatedTime = getTime(estimatedTime);

                                flightResult.append("\n" + "Flight Number: " + flight +
                                        "\n" + "Gate: " + gate +
                                        "\n" + "Time: " + properEstimatedTime);
                                flightData flightData = new flightData(flight, gate, properEstimatedTime);
                                mDatabaseReference.child(flight).setValue(flightData);
                                break;
                            } else if (AD == 1) {
                                JSONObject getInfo = response.getJSONObject(i).optJSONObject("arrival");
                                String estimatedTime = getInfo.getString("estimatedTime");
                                String properEstimatedTime = getTime(estimatedTime);

                                flightResult.append("\n" + "Flight Number: " + flight +
                                        "\n" + "Time: " + properEstimatedTime);
                                flightData flightData = new flightData(flight, "", properEstimatedTime);
                                mDatabaseReference.child(flight).setValue(flightData);
                                break;
                            }
                        }
                    }
                    if (foundFlight == 0){
                        flightResult.append("Flight not found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error :" + error.toString());
            }
        });

        mRequestQueue.add(jsonArrayRequest);
    }


    //converts datetime to a string that can be displayed
    private String getTime(String dateTime) {
        String result = null;
        String re1 = ".*?";    // Non-greedy match on filler
        String re2 = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1}))[-:\\/.](?:[0]?[1-9]|[1][012])[-:\\/.](?:(?:\\d{1}\\d{1})))(?![\\d])";    // DDMMYY 1
        String re3 = ".*?";    // Non-greedy match on filler
        String re4 = "((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";    // HourMinuteSec 1

        Pattern p = Pattern.compile(re1 + re2 + re3 + re4, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(dateTime);
        if (m.find()) {
            String ddmmyy1 = m.group(1);
            String time1 = m.group(2);
            result = (time1.toString() + " " + ddmmyy1.toString() + "\n");
        }
        return result;
    }

}


//class for flight data object
class flightData{
    public String flight;
    public String gate;
    public String properEstimatedTime;

    public flightData(){

    }

    public flightData(String flight, String gate, String properEstimatedTime){
        this.flight = flight;
        this.gate = gate;
        this.properEstimatedTime = properEstimatedTime;
    }
}
