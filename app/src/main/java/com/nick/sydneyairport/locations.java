


package com.nick.sydneyairport;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.TextView;
import android.widget.Toast;

import android.location.Location;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.io.Serializable;

class Place{
    String name;
    String description;
    String[] nodes;
    int id;

    Place(String name, String description, String[] nodes, int id){
        this.name = name;
        this.description = description;
        this.nodes = nodes;
        this.id = id;
    }
}


public class locations extends AppCompatActivity {
    /**
     * Recycler
     */
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<Place> myDataset;
    private TextView mTextMessage;

    String flightNO;
    int AD;
    public Place end;

    private static final String TAG = "locations";
    /**
     * Location
     */

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    private FusedLocationProviderClient mFusedLocationClient;
    //public Location mLastLocation;
    public Double[] currentLocation = new Double[2];
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 300 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 300000;

    private static final int REQUEST_CHECK_SETTINGS = 100;

    //private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    private boolean mRequestingLocationUpdates;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.putExtra("key", flightNO);
                    intent.putExtra("key2",AD);
                    startActivity(intent);
                    return true;
                case R.id.navigation_flights:
                    //mTextMessage.setText(R.string.title_flights);
                    Intent intent2 = new Intent(getApplicationContext(), flights.class);
                    intent2.putExtra("key", flightNO);
                    intent2.putExtra("key2",AD);
                    startActivity(intent2);
                    return true;
                case R.id.navigation_map:
                    //mTextMessage.setText(R.string.title_map);
                    Intent intent4 = new Intent(getApplicationContext(), mapsActivity.class);
                    startActivity(intent4);
                    return true;
                case R.id.navigation_stores:
                    //mTextMessage.setText(R.string.title_stores);
                    Intent intent5 = new Intent(getApplicationContext(), locations.class);
                    startActivity(intent5);
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        flightNO = getIntent().getStringExtra("key");
        AD = getIntent().getIntExtra("key2",3);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        String[] nullArray = {"nullNode"};
        end = new Place("nullName", "nullDescription", nullArray, -1);


        initialiseData();
        initialiseAdapter();
        init();

        mRecyclerView.addOnItemTouchListener(new RecyclerViewClick(getApplicationContext(), mRecyclerView, new RecyclerViewClick.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                end = myDataset.get(position);
                Toast.makeText(getApplicationContext(), end.name + " is selected!", Toast.LENGTH_SHORT).show();

                ArrayList<String> location = new ArrayList<String>();

                if(currentLocation[0] != null) {
                    location.add(currentLocation[0].toString());
                    location.add(currentLocation[1].toString());
                }else{
                    onStart();
                    location.add(currentLocation[0].toString());
                    location.add(currentLocation[1].toString());

                }

                ArrayList<String> endNodes = new ArrayList<>(Arrays.asList(end.nodes));
                Intent intent = new Intent(getApplicationContext(), mapsActivity.class);
                intent.putStringArrayListExtra("key",endNodes);
                intent.putStringArrayListExtra("key2", location);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

  private void init() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        if (mLocationSettingsRequest != null){
            System.out.println("mLocationSettingsRequest NOT NULL");
        }else{
            System.out.println("mLocationSettingsRequest IS NULL");
        }
    }

    /**
     * Restoring values from saved instance state
     */

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */

    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            currentLocation[0] = mCurrentLocation.getLatitude();
            currentLocation[1] = mCurrentLocation.getLongitude();

            if (end.nodes != null) {
                if (end.name != "nullName") {

                    ArrayList<String> location = new ArrayList<String>();

                    location.add(currentLocation[0].toString());
                    location.add(currentLocation[1].toString());

                    ArrayList<String> endNodeList = new ArrayList<String>(Arrays.asList(end.nodes));


                    Intent intent = new Intent(getApplicationContext(), mapsActivity.class);
                    intent.putExtra("key", endNodeList);
                    intent.putStringArrayListExtra("key2", location);

                    startActivity(intent);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);

    }


    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(locations.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(locations.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                        //toggleButtons();
                    }
                });
    }


    public void showLastKnownLocation() {
        if (mCurrentLocation != null) {
            Toast.makeText(getApplicationContext(), "Lat: " + mCurrentLocation.getLatitude()
                    + ", Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Last known location is not available!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }

        updateLocationUI();
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */


    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

 
    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(locations.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }
//
    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                startLocationUpdates();
            } else {
                // Permission denied.
                                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Build intent that displays the App settings screen.
                        Intent intent = new Intent();
                        intent.setAction(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }
    }
//
    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            startLocationUpdates();
        }
    }

    private void initialiseData(){
        myDataset = new ArrayList<>();
        //Loop through dataset and add places
        String[] p1 = {"n1"};
        String[] p2 = {"n2"};
        String[] p3 = {"n3"};
        String[] p4 = {"n4"};
        String[] p5 = {"n5"};
        String[] p6 = {"n6"};
        String[] p7 = {"n7"};
        String[] p8 = {"n8"};
        String[] p9 = {"n9"};
        String[] p10 = {"n10"};
        String[] p11 = {"n11"};
        String[] p12 = {"n12"};
        String[] p13 = {"n13"};
        String[] p14 = {"n14"};
        String[] p15 = {"n15"};
        String[] p16 = {"n16"};
        String[] p17 = {"n17"};
        String[] p18 = {"n18"};
        String[] p19 = {"n19"};
        String[] p20 = {"n20"};
        String[] p21 = {"n21"};
        String[] p22 = {"n22"};
        String[] p23 = {"n23"};
        String[] p24 = {"n24"};
        String[] p25 = {"n25"};
        String[] p26 = {"n26"};
        String[] p27 = {"n27"};
        String[] p29 = {"n29"};
        String[] p30 = {"n30"};
        String[] p31 = {"n31"};
        String[] p32 = {"n32"};
        String[] p33 = {"n33"};
        String[] p34 = {"n34"};
        String[] p35 = {"n35"};
        String[] p36 = {"n36","102"};
        String[] p37 = {"n37"};
        String[] p38 = {"n38"};
        String[] p39 = {"n39"};
        String[] p40 = {"n40"};
        String[] p41 = {"n41"};
        String[] p42 = {"n42"};
        String[] p43 = {"n43"};
        String[] p44 = {"n44"};
        String[] p45 = {"n45"};
        String[] p400 = {"400"};
        String[] p47 = {"n47"};
        String[] p48 = {"n48"};
        String[] p49 = {"n49"};
        String[] p52 = {"n52"};
        String[] p54G = {"n54","n56","n58","n59","n61","n62","n63","n64","n65","n66"};
        String[] p67 = {"n67"};
        String[] p71 = {"n71"};
        String[] p72 = {"n72"};
        String[] p73 = {"n73"};
        String[] p75 = {"n75"};
        String[] p77 = {"n78"};
        String[] p79 = {"n79"};
        String[] p80 = {"n80"};
        String[] p81 = {"n81"};
        String[] p83 = {"n83"};
        String[] p85 = {"n85"};
        String[] p87 = {"n87"};
        String[] p89 = {"n89"};
        String[] p90 = {"n90"};
        String[] p91 = {"n91"};
        String[] p92 = {"n92"};
        String[] p94 = {"n94"};
        String[] p96 = {"n96"};
        String[] p97 = {"n97"};
        String[] p98 = {"n98"};
        String[] p100 = {"n100"};
        String[] p103 = {"n103","n45","n106"};
        String[] p107T = {"n107","n108","n109","n110","n111","n112","n113","n114","n115","n116","n117","n118","n119"};
        String[] p107A = {"n107","n109","n110","n114","n115","n116","n117","n118","n119"};
        String[] p107P = {"n107","n109","n110","n111","n113","n114","n116","n117","n118","n119"};
        String[] p110S = {"n110"};
        String[] p140 = {"n140"};
        String[] p141 = {"n141","n215"};
        String[] p143 = {"n143","n145","n146","n147","n149","n153"};
        String[] p90H = {"n90","n157"};
        String[] p158 = {"n158"};
        String[] p161 = {"n161"};
        String[] p162 = {"n162"};
        String[] p163 = {"n163"};
        String[] p164 = {"n164"};
        String[] p166 = {"n166"};
        String[] p167 = {"n167"};
        String[] p90I = {"n90","n177","n178","n179","n180"};
        String[] p181 = {"n181"};
        String[] p183 = {"n183"};
        String[] p184 = {"n184"};
        String[] p185 = {"n185"};
        String[] p186 = {"n186"};
        String[] p187 = {"n187"};
        String[] p188 = {"n188"};
        String[] p189 = {"n189"};
        String[] p190 = {"n190"};
        String[] p191 = {"n191"};
        String[] p192 = {"n192"};
        String[] p194 = {"n194"};
        String[] p195 = {"n195"};
        String[] p196 = {"n196"};
        String[] p197 = {"n197"};
        String[] p198 = {"n198"};
        String[] p199 = {"n199"};
        String[] p202 = {"n202"};
        String[] p216 = {"n216"};



        myDataset.add(new Place("Check-in Desk A","", p1,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk B","",p2,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk C","",p3,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Qantas Check-in desk","",p4,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk D","",p5,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk E","",p6,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk F","",p7,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk G","",p8,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk H","",p9,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk J","",p10,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Check-in Desk K","",p11,R.drawable.ic_launcher_background));
        myDataset.add(new Place("BBQ Chicken","Bars & Cafés: BBQ",p12,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Better Burger","Casual Dining: Burger",p13,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Boost","Drink",p14,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Bridge Bar","Bars & Café",p15,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Chatime","Drink",p16,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Coast Café","Bars & Café",p17,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Heineken House","Bars & Café",p18,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Hungary Jacks's","Quick Eats: Burgers",p19,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Kitchen by Mike","Bars & Café",p20,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Luxe Bakery","Bars & Café",p21,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Mach2","Casual Dining",p22,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Mad Mex","Quick Eats",p23,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Mcdonald's","Quick Eats: Fast food",p24,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Mrs Fields Bakery Café","Bars & Café",p25,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Oliver Brown","Quick Eats: Café",p26,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Roll'd","Casual Dining",p27,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Soul Origin","Quick Eats",p29,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Starbucks","Bars & Café: Drink",p30,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Subway","Quick Eats",p31,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Sushia","Bars & Café",p32,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Taste of Thai","Casual Dining",p33,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Terrace Chinese Kitchen","Casual Dining",p34,R.drawable.ic_launcher_background));
        myDataset.add(new Place("The Bisto by Wolfgane Puck","Bars & Café",p35,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Toby's Estate","Quick Eats",p36,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Top Juice","Drink",p37,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Viaggio Espressp + Bar","Bars & Café",p38,R.drawable.ic_launcher_background));
        myDataset.add(new Place("YO! Sushi","Quick Eats",p39,R.drawable.ic_launcher_background));
        myDataset.add(new Place("A Little Something","Duty-Free",p40,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Amcal Pharmacy","Travel Essentials",p41,R.drawable.ic_launcher_background));
        myDataset.add(new Place("ANZ Bank","Services and Currency",p42,R.drawable.ic_launcher_background));
        myDataset.add(new Place("APM Monaco","Fashion and Accessories",p43,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Australian Way","Souvenirs",p44,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Australia Post","Services and Currency",p45,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Bally","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Burberry","Fashion and Accessories",p47,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Coach","Fashion and Accessories",p48,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Emporio Armani","Fashion and Accessories",p49,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Ermenegildo Zegna","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Fendi","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Flight Centre","Services and Currency",p52,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Furla","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Global Exchange","Services and Currency",p54G,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gucci","Fashion and Accessories",p67,R.drawable.ic_launcher_background));
        myDataset.add(new Place("HEINEMAN Tax & Duty Free","Duty-Free",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Hermes","Fashion and Accessories",p71,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Hugo Boss","Fashion and Accessories",p72,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Ice Breaker","Fashion and Accessories",p73,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Kailis","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Kate Spade New York","Fashion and Accessories",p75,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Lacosta","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Max Mara","Fashion and Accessories",p77,R.drawable.ic_launcher_background));
        myDataset.add(new Place("MCM","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Merino Collection","Fashion and Accessories",p79,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Merino Collection","Fashion and Accessories",p80,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Michael Kors","Fashion and Accessories",p81,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Montblanc","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Oroton","Fashion and Accessories",p83,R.drawable.ic_launcher_background));
        myDataset.add(new Place("R.M. Williams","Fashion and Accessories",p85,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Rolling Luggage","Travel Essentials",p87,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Salvatore Ferragamo","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Secure Travel","Services and Currency",p89,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Secure Travel","Services and Currency",p90,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Stretch Studio","Services and Currency",p91,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Sunglass Hut","Fashion and Accessories",p92,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Surf Dive 'n' Ski","Fashion and Accessories",p94,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Swarovski","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Think Australia","Souvenirs",p96,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Think Sydney","Souvenirs",p97,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Tiffany & Co","Fashion and Accessories",p98,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Toscow Australia","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("TUMI","Fashion and Accessories",p100,R.drawable.ic_launcher_background));
        myDataset.add(new Place("UGG Australia","Fashion and Accessories",p400,R.drawable.ic_launcher_background));
        myDataset.add(new Place("WHSmith","Travel Essentials",p103,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Toilets","",p107T,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Assessible toilets","",p107A,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Parents room","",p107P,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Shower","Must provide your own soap, shampoo, conditioner, etc",p110S,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Tourist refund scheme","GST and WET refund",p140,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Oversized baggage","Oversized baggage deposit",p141,R.drawable.ic_launcher_background));
        myDataset.add(new Place("ATM","",p143,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Telephone","",p90H,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Internet Koisk","",p158,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Air New Zealand Lounge","",p161,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Emirates Lounge","",p162,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Etihad Lounge","",p163,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Qantas Club Lounge","",p164,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Singapore Airlines Silver Kris Lounge","",p161,R.drawable.ic_launcher_background));
        myDataset.add(new Place("American Express Lounge","",p166,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Skyteam Lounge","",p167,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Informational Desk","Airport Information",p90I,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 36","",p181,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 37","",p181,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 35","",p183,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 33","",p184,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 34","",p185,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 32","",p186,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 31","",p187,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 30","",p188,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 24","",p189,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 25","",p190,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 10","",p191,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 9","",p192,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 8","",p192,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 50","",p194,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 51","",p195,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 53","",p196,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 54","",p197,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 55","",p198,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 56","",p199,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 57","",p198,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 58","",p198,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 59","",p202,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 60","",p202,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 61","",p202,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 62","",p202,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Gate 63","",p202,R.drawable.ic_launcher_background));
        myDataset.add(new Place("Passport control","Only passengers on a flight are allowed beyond this points",p216,R.drawable.ic_launcher_background));
    }

    private void initialiseAdapter(){
        mAdapter = new MyAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
    }
}
