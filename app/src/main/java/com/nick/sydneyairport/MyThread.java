package com.nick.sydneyairport;



public class MyThread extends Thread {

    private int mX;
    private int mY;

    public MyThread(int viewWidth, int viewHeight) {
        //super()
        mX = viewWidth / 2;
        mY = viewHeight / 2;

    }

    /**
     * Update the coordinates of the map.
     *
     * @param newX Changed value for x coordinate.
     * @param newY Changed value for y coordinate.
     */
    public void update(int newX, int newY) {
        mX = newX;
        mY = newY;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

}