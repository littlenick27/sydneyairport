package com.nick.sydneyairport;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class placeCardView extends Activity{

    TextView name;
    TextView description;
    ImageView id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.places_cv);
        name = (TextView) findViewById(R.id.name);
        description = (TextView) findViewById(R.id.description);
        id = (ImageView) findViewById(R.id.id);

    }
}
