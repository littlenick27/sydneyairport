package com.nick.sydneyairport;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button3;
    Button button2;
    private EditText flightno;
    public String flightNO;
    public int AD;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this.getApplicationContext();
        flightno = findViewById(R.id.editText);
        button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flightno.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter the Flight NO", Toast.LENGTH_SHORT).show();
                } else {
                    flightNO = flightno.getText().toString();
                    AD = 0;
                    Toast.makeText(getApplicationContext(), "Name -  " + flightno.getText().toString(),Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    public void gotoStoresActivity (View view) {

        // Create an Intent to start the second activity
        Intent intent1 = new Intent(this, locations.class);
        intent1.putExtra("key",flightNO);
        intent1.putExtra("key2",AD);
        // Start the new activity.
        startActivity(intent1);
    }

    public void gotoFlightsActivity (View view) {

        // Create an Intent to start the second activity
        Intent intent2 = new Intent(this, flights.class);
        intent2.putExtra("key",flightNO);
        intent2.putExtra("key2",AD);
        // Start the new activity.
        startActivity(intent2);
    }


}



