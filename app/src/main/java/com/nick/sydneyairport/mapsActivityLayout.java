package com.nick.sydneyairport;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class mapsActivityLayout extends SurfaceView implements Runnable {


    Thread thread = null;
    MyThread draw = null;
    boolean canDraw = false;
    Path path;

    Bitmap backGround;
    Bitmap rotatedBackGround;
    SurfaceHolder surfaceHolder;
    Context mContext;
    Paint paint;



    int bitmapX;
    int bitmapY;
    int viewWidth;
    int viewHeight;


    static double RADIANS = 57.2957795;

    static double oLat = -33.9370932;
    static double oLon = 151.1673065;

    ArrayList<ArrayList<Double>> walkingPath;


    Paint red_paintbrush_fill, blue_paintbrush_fill, green_paintbrush_fill;
    Paint red_paintbrush_stroke, blue_paintbrush_stroke, green_paintbrush_stroke;


    //initialise all the variables
    public mapsActivityLayout(Context context, ArrayList<ArrayList<Double>> walkingPath) {
        super(context);

        this.walkingPath = walkingPath;
        mContext = context;
        surfaceHolder = getHolder();
        paint = new Paint();
        path = new Path();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        viewWidth = w;
        viewHeight = h;

        draw = new MyThread(viewWidth, viewHeight);

        // Set font size proportional to view size.
        paint.setTextSize(viewHeight / 5);
        //walkingPath = mapsActivity.route();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = true;
        options.inMutable = true;

        Matrix matrix = new Matrix();
        matrix.postRotate(270);
        matrix.preScale(1.0f, -1.0f);
        rotatedBackGround = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.mapimage, options);
        backGround = Bitmap.createBitmap(rotatedBackGround, 0, 0, rotatedBackGround.getWidth(), rotatedBackGround.getHeight(), matrix, true);
        System.out.println("width" + backGround.getWidth());
        System.out.println("height" + backGround.getHeight());
        System.out.println("scaled width" + rotatedBackGround.getWidth());
        System.out.println("scaled height" + rotatedBackGround.getHeight());
        setUpBitmap();
    }


    @Override
    public void run() {
        Canvas canvas;
        prepPaintBrushes();
        while (canDraw) {
            //draw stuff
            if (surfaceHolder.getSurface().isValid()) {
                int x = draw.getX();
                int y = draw.getY();
                canvas = surfaceHolder.lockCanvas();
                canvas.save();
                canvas.drawBitmap(backGround, bitmapX, bitmapY, paint);

                float currentX = (float) getXY(walkingPath.get(0).get(0), walkingPath.get(0).get(1)).get(0);//+bitmapX;
                float currentY = (float) getXY(walkingPath.get(0).get(0), walkingPath.get(0).get(1)).get(1);//+bitmapY;
                System.out.println("Walkk" + walkingPath);
                float destX = (float) getXY(walkingPath.get(walkingPath.size() - 1).get(0), walkingPath.get(walkingPath.size() - 1).get(1)).get(0);// +bitmapX;
                float destY = (float) getXY(walkingPath.get(walkingPath.size() - 1).get(0), walkingPath.get(walkingPath.size() - 1).get(1)).get(1);// +bitmapY;
                System.out.println("walkP: 0: " + walkingPath.get(walkingPath.size() - 1).get(0));
                System.out.println("walkP: 1: " + walkingPath.get(walkingPath.size() - 1).get(1));

                System.out.println("destOX: " + destX);
                System.out.println("destOY: " + destY);

                float drawCurrentX = 0;
                float drawCurrentY = 0;

                float drawDestX = 0;
                float drawDestY = 0;
                System.out.println("bgHW" + (backGround.getWidth())/2);
                System.out.println("bgHH" + (backGround.getHeight())/2);

                if (currentX < 0){
                    drawCurrentX =  (backGround.getWidth())/2 + currentX + bitmapX ;
                }

                if (currentY < 0){
                    drawCurrentY = (backGround.getHeight())/2 - currentY + bitmapY;
                }


                if (currentX > 0){
                    drawCurrentX = (backGround.getWidth())/2 + currentX + bitmapX;
                }


                if (currentY > 0){
                    drawCurrentY = (backGround.getHeight())/2 - currentY + bitmapY;
                }


                if (destX < 0){
                    System.out.println("in here -x");
                    drawDestX = (backGround.getWidth())/2 + destX + bitmapX;
                }

                if (destY < 0){
                    System.out.println("in here -y");
                    drawDestY = (backGround.getHeight())/2 - destY  + bitmapY;
                }

                if (destX > 0){
                    System.out.println("in here +x");
                    drawDestX = (backGround.getWidth())/2 + destX + bitmapX;
                }

                if (destY > 0){
                    System.out.println("in here +y");
                    drawDestY = (backGround.getHeight())/2 - destY + bitmapY;
                }

                System.out.println("destNX: " + drawDestX);
                System.out.println("destNY: " + drawDestY);

                canvas.drawCircle(drawCurrentX, drawCurrentY, 20, red_paintbrush_fill);
                canvas.drawCircle(drawDestX, drawDestY, 20, green_paintbrush_fill);

                for(int i = 0; i < walkingPath.size()-2; i++){
                    float aX = getXY(walkingPath.get(i).get(0), walkingPath.get(i).get(1)).get(0);// + bitmapX;
                    float aY = getXY(walkingPath.get(i).get(0), walkingPath.get(i).get(1)).get(1);// + bitmapY;

                    float bX = getXY(walkingPath.get(i+1).get(0), walkingPath.get(i+1).get(1)).get(0);// + bitmapX;
                    float bY = getXY(walkingPath.get(i+1).get(0), walkingPath.get(i+1).get(1)).get(1);// + bitmapY;

                    float drawAX = 0;
                    float drawAY = 0;

                    float drawBX = 0;
                    float drawBY = 0;


                    if (aX < 0){
                        drawAX = (backGround.getWidth())/2 + aX + bitmapX;
                    }

                    if (aY < 0){
                        drawAY = (backGround.getHeight())/2 - aY + bitmapY;
                    }

                    if (aX > 0){
                        drawAX = (backGround.getWidth())/2 + aX + bitmapX;
                    }

                    if (aY > 0){
                        drawAY = (backGround.getHeight())/2 - aY + bitmapY;
                    }

                    if (bX < 0){
                        drawBX = (backGround.getWidth())/2 + bX + bitmapX;
                    }

                    if (bY < 0){
                        drawBY = (backGround.getHeight())/2 - bY + bitmapY;
                    }


                    if (bX > 0){
                        drawBX = (backGround.getWidth())/2 + bX + bitmapX;
                    }

                    if (bY > 0){
                        drawBY = (backGround.getHeight())/2 - bY + bitmapY;
                    }
                    System.out.println("XLines" + drawAX);
                    System.out.println("YLines" + drawAY);

                    canvas.drawLine(drawAX, drawAY, drawBX, drawBY, blue_paintbrush_stroke);
                }

                path.rewind();
                canvas.restore();
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }


    private void updateFrame(int newX, int newY) {
        draw.update(newX, newY);
    }

    /**
     * Calculates a  location for the bitmap
     */
    private void setUpBitmap() {
        bitmapX = (int) Math.floor(
                Math.random() * (viewWidth - backGround.getWidth()));
        bitmapY = (int) Math.floor(
                Math.random() * (viewHeight - backGround.getHeight()));
    }


    public void pause() {
        canDraw = false;
        while (true) {
            try {
                thread.join();
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void resume() {
        canDraw = true;
        thread = new Thread(this);
        thread.start();
    }

    private void prepPaintBrushes() {
        red_paintbrush_fill = new Paint();
        red_paintbrush_fill.setColor(Color.RED);
        red_paintbrush_fill.setStyle(Paint.Style.FILL);

        blue_paintbrush_fill = new Paint();
        blue_paintbrush_fill.setColor(Color.BLUE);
        blue_paintbrush_fill.setStyle(Paint.Style.FILL);

        green_paintbrush_fill = new Paint();
        green_paintbrush_fill.setColor(Color.GREEN);
        green_paintbrush_fill.setStyle(Paint.Style.FILL);

        red_paintbrush_stroke = new Paint();
        red_paintbrush_stroke.setColor(Color.RED);
        red_paintbrush_stroke.setStyle(Paint.Style.STROKE);
        red_paintbrush_stroke.setStrokeWidth(10);

        blue_paintbrush_stroke = new Paint();
        blue_paintbrush_stroke.setColor(Color.BLUE);
        blue_paintbrush_stroke.setStyle(Paint.Style.STROKE);
        blue_paintbrush_stroke.setStrokeWidth(10);

        green_paintbrush_stroke = new Paint();
        green_paintbrush_stroke.setColor(Color.GREEN);
        green_paintbrush_stroke.setStyle(Paint.Style.STROKE);
        green_paintbrush_stroke.setStrokeWidth(10);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        // Invalidate() is inside the case statements because there are
        // many other motion events, and we don't want to invalidate
        // the view for those.
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setUpBitmap();
                // Set coordinates of map.
                updateFrame((int) x, (int) y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                // Updated coordinates for map.
                setUpBitmap();
                updateFrame((int) x, (int) y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                // Updated coordinates for map.
                updateFrame((int) x, (int) y);
                invalidate();
                break;
            default:
                // Do nothing.
        }
        return true;
    }


    /**
     * The code below was converted from JS logic from the NDSF coordinate conversion utility at
     * http://www.whoi.edu/marine/ndsf/cgi-bin/NDSFutility.cgi?form=0&from=LatLon&to=XY
     */
    private double deg2rad(double x)
    {

        return (x/RADIANS);
    }

    private double METERS_DEGLON(double x)
    {

        double d2r=deg2rad(x);
        return((111415.13 * Math.cos(d2r))- (94.55 * Math.cos(3.0*d2r)) + (0.12 * Math.cos(5.0*d2r)));

    }

    private double METERS_DEGLAT(double x)
    {

        double d2r=deg2rad(x);
        return(111132.09 - (566.05 * Math.cos(2.0*d2r))+ (1.20 * Math.cos(4.0*d2r)) - (0.002 * Math.cos(6.0*d2r)));

    }

    private ArrayList<Float> translate_coordinates(double olat, double olon, double dlat, double dlon)
    {   System.out.println("dlon: " + dlon + " dlat: " + dlat + " olon: " + olon + " olat: " + olat);
        ArrayList<Float> drawxy = new ArrayList<Float>();
        double xx, yy, r, ct, st, angle, x, y;
        angle = 0;

        xx = (dlon - olon)*METERS_DEGLON(olat);
        yy = (dlat - olat)*METERS_DEGLAT(olat);

        r = Math.sqrt(xx*xx + yy*yy);

        ct = xx/r;
        st = yy/r;
        x = r * ( (ct * Math.cos(angle)) + (st * Math.sin(angle)));// + backGround.getWidth()/2;
        y = r * ( (st * Math.cos(angle)) - (ct * Math.sin(angle)));// + backGround.getHeight()/2;

        drawxy.add((float) x);
        drawxy.add((float) y);

        return(drawxy);
    }



    private ArrayList<Float> getXY(double dLat, double dLon)
    {
        ArrayList<Float> xy = translate_coordinates(oLat, oLon, dLat, dLon);
        System.out.println("drawxyX:" + xy.get(0));
        System.out.println("drawxyY:" + xy.get(1));

        return xy;
    }
}
