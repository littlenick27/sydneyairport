/**
 * This code is a modified version of the Dijkstra (https://rosettacode.org/wiki/Dijkstra%27s_algorithm)
 * and Ray Casting (https://rosettacode.org/wiki/Ray-casting_algorithm) algorithm from Rosetta Code.
 * Rosetta code allow for free use of their code for any means unless other stated.
 */


package com.nick.sydneyairport;


import java.util.*;

import static java.lang.Math.*;


public class Dijkstra {



    /**
     * Initialise nodes
     */
    public Dijkstra()
    {

    }
    public Dijkstra(node current)
    {
        currentLocation = current.name;
        currentNode = current;
        Graph.Edge[] newGraph = {
                new Graph.Edge("n207","n1",9.784775236),
                new Graph.Edge("n207","n208",80.20995109),
                new Graph.Edge("n1","n215",34.64958661),
                new Graph.Edge("n1","n113",39.06419748),
                new Graph.Edge("n1","n2",35.99677669),
                new Graph.Edge("n215","n113",26.2613474),
                new Graph.Edge("n113","n221",46.55324045),
                new Graph.Edge("n113","n2",45.58474439),
                new Graph.Edge("n62","n6",57.28178806),
                new Graph.Edge("n62","n216",15.63506538),
                new Graph.Edge("n2","n3",24.15404196),
                new Graph.Edge("n3","n177",18.04757777),
                new Graph.Edge("n177","n4",4.812529279),
                new Graph.Edge("n4","n216",72.40626389),
                new Graph.Edge("n4","n208",8.444033786),
                new Graph.Edge("n4","n5",7.462787245),
                new Graph.Edge("n4","n222",45.94892524),
                new Graph.Edge("n208","n209",29.15749135),
                new Graph.Edge("n5","n213",15.48622555),
                new Graph.Edge("n5","n223",46.58884582),
                new Graph.Edge("n221","n222",23.83498732),
                new Graph.Edge("n221","n3",37.79852739),
                new Graph.Edge("n222","n223",7.168998819),
                new Graph.Edge("n223","n62",25.30967931),
                new Graph.Edge("n213","n209",4.739886718),
                new Graph.Edge("n213","n6",16.02137139),
                new Graph.Edge("n209","n210",30.53967488),
                new Graph.Edge("n6","n214",13.88943973),
                new Graph.Edge("n210","n214",4.552717901),
                new Graph.Edge("n210","n211",87.78367384),
                new Graph.Edge("n214","n7",15.56285027),
                new Graph.Edge("n7","n8",27.3571853),
                new Graph.Edge("n8","n90",23.52903687),
                new Graph.Edge("n8","n258",34.11396264),
                new Graph.Edge("n90","n145",5.952694268),
                new Graph.Edge("n145","n9",19.53613545),
                new Graph.Edge("n146","n259",27.24330038),
                new Graph.Edge("n145","n146",13.96224197),
                new Graph.Edge("n259","n103",17.12072501),
                new Graph.Edge("n103","n9",35.51576728),
                new Graph.Edge("n9","n42",20.34974784),
                new Graph.Edge("n9","n26",66.5401447),
                new Graph.Edge("n9","n270",53.26959089),
                new Graph.Edge("n270","n26",13.27673382),
                new Graph.Edge("n8","n145",29.47473065),
                new Graph.Edge("n9","n145",19.53613545),
                new Graph.Edge("n10","n11",24.90769635),
                new Graph.Edge("n10","n220",62.61960847),
                new Graph.Edge("n45","n220",34.44180578),
                new Graph.Edge("n11","n141",16.506213),
                new Graph.Edge("n11","n212",11.62116553),
                new Graph.Edge("n212","n211",44.695743),
                new Graph.Edge("n9","n211",12.22359877),
                new Graph.Edge("n216","n260",20.84761841),
                new Graph.Edge("n73","n260",9.321598361),
                new Graph.Edge("n263","n260",10.53731109),
                new Graph.Edge("n92","n263",7.714545686428179),
                new Graph.Edge("n92","n262",16.13124248),
                new Graph.Edge("n263","n262",16.13124248),
                new Graph.Edge("n260","n149",4.898830423),
                new Graph.Edge("n263","n261",10.62715503),
                new Graph.Edge("n261","n92",10.62715503),
                new Graph.Edge("n261","n87" ,6.498422818),
                new Graph.Edge("n262","n94",9.935579239),
                new Graph.Edge("n262","n29",10.17133052),
                new Graph.Edge("n262","n264",14.30307175),
                new Graph.Edge("n263","n111",40.63190525),
                new Graph.Edge("n27","n264",7.570688562),
                new Graph.Edge("n264","n36",8.204783465),
                new Graph.Edge("n264","n16",13.34781435),
                new Graph.Edge("n36","n16",5.964037785),
                new Graph.Edge("n36","n25",11.1764558),
                new Graph.Edge("n16","n25",10.6288509),
                new Graph.Edge("n16","n265",16.05424327),
                new Graph.Edge("n36","n265",21.47512739),
                new Graph.Edge("n33","n265",6.873797786),
                new Graph.Edge("n265","n103",23.59978725),
                new Graph.Edge("n265","n178",10.45876463),
                new Graph.Edge("n178","n37",12.20589196),
                new Graph.Edge("n37","n19",10.84207028),
                new Graph.Edge("n37","n147",19.91057508),
                new Graph.Edge("n37","n266",34.06095562),
                new Graph.Edge("n23","n266",12.98539971),
                new Graph.Edge("n59","n266",4.358550942),
                new Graph.Edge("n178","n103",25.10554404),
                new Graph.Edge("n268","n267",10.43979102),
                new Graph.Edge("n31","n267",8.52475846),
                new Graph.Edge("n264","n267",6.116221284),
                new Graph.Edge("n267","n36",7.133082712),
                new Graph.Edge("n267","n16",9.607252582),
                new Graph.Edge("n268","n36",13.35388937),
                new Graph.Edge("n268","n16",9.8773589),
                new Graph.Edge("n268","n19",9.087107512),
                new Graph.Edge("n268","n25",20.39641479),
                new Graph.Edge("n269","n22",10.43417858),
                new Graph.Edge("n269","n266",11.65291635),
                new Graph.Edge("n270","n269",13.48918582),
                new Graph.Edge("n59","n266",4.358550942),
                new Graph.Edge("n270","n220",59.99562088),
                new Graph.Edge("n220","n143",22.16180219),
                new Graph.Edge("n11","n143",52.71434316),
                new Graph.Edge("n11","n141",16.506213),
                new Graph.Edge("n143","n220",22.16180219),
                new Graph.Edge("n220","n158",2.774187167),
                new Graph.Edge("n158","n141",40.11966304),
                new Graph.Edge("n220","n110",9.176910072),
                new Graph.Edge("n220","n110",9.176910072),
                new Graph.Edge("n220","n112",183.3604201),
                new Graph.Edge("n400","n179",35.88532295),
                new Graph.Edge("n400","n225",39.30980568),
                new Graph.Edge("n400","n224",39.80100366),
                new Graph.Edge("n224","n54",40.09281147),
                new Graph.Edge("n15","n54",11.76528443),
                new Graph.Edge("n192","n114",22.30528775),
                new Graph.Edge("n54","n191",13.70886525),
                new Graph.Edge("n54","n229",41.62514983),
                new Graph.Edge("n229","n13",7.003031692),
                new Graph.Edge("n231","n229",16.02277755),
                new Graph.Edge("n231","n35",8.783529648),
                new Graph.Edge("n231","n58",13.12071829),
                new Graph.Edge("n224","n179",52.81545226),
                new Graph.Edge("n224","n225",43.43928991),
                new Graph.Edge("n224","n164",22.38634903),
                new Graph.Edge("n58","n164",21.47680589),
                new Graph.Edge("n231","n232",18.30416131),
                new Graph.Edge("n232","n18",7.289295568),
                new Graph.Edge("n232","n233",12.26555001),
                new Graph.Edge("n233","n20",10.1615798),
                new Graph.Edge("n233","n226",18.22175529),
                new Graph.Edge("n226","n234",14.53587579),
                new Graph.Edge("n235","n234",7.441019224),
                new Graph.Edge("n91","n235",2.313662702),
                new Graph.Edge("n226","n180",14.5798274),
                new Graph.Edge("n226","n32",22.97441359),
                new Graph.Edge("n180","n32",19.34118993),
                new Graph.Edge("n43","n32",7.297327171),
                new Graph.Edge("n66","n32",9.959134033),
                new Graph.Edge("n66","n34",15.34122901),
                new Graph.Edge("n66","n12",7.316443876),
                new Graph.Edge("n34","n12",8.807607492),
                new Graph.Edge("n66","n43",11.54413891),
                new Graph.Edge("n34","n32",15.45068727),
                new Graph.Edge("n12","n32",12.50674096),
                new Graph.Edge("n66","n43",11.54413891),
                new Graph.Edge("n34","n43",22.19958737),
                new Graph.Edge("n12","n43",17.20369052),
                new Graph.Edge("n34","n12",8.807607492),
                new Graph.Edge("n66","n188",8.936607746),
                new Graph.Edge("n180","n34",30.39029088),
                new Graph.Edge("n180","n12",22.39741668),
                new Graph.Edge("n180","n43",13.67529811),
                new Graph.Edge("n180","n66",15.15743846),
                new Graph.Edge("n188","n236",21.21611567),
                new Graph.Edge("n236","n79",153.9257521),
                new Graph.Edge("n236","n237",7.440413648),
                new Graph.Edge("n237","n153",4.202762599),
                new Graph.Edge("n237","n118",6.825776983),
                new Graph.Edge("n237","n187",15.17170094),
                new Graph.Edge("n186","n187",23.39267337),
                new Graph.Edge("n238","n187",18.08673334),
                new Graph.Edge("n238","n38",9.8151214),
                new Graph.Edge("n238","n239",30.38747365),
                new Graph.Edge("n239","n106",8.298162264),
                new Graph.Edge("n239","n240",29.26823885),
                new Graph.Edge("n119","n240",4.497953856),
                new Graph.Edge("n184","n183",29.44215047),
                new Graph.Edge("n183","n181",21.29836049),
                new Graph.Edge("n225","n241",2.50084375),
                new Graph.Edge("n98","n241",10.66904853),
                new Graph.Edge("n242","n241",15.19277296),
                new Graph.Edge("n242","n49",10.17310237),
                new Graph.Edge("n242","n243",9.711743157),
                new Graph.Edge("n72","n243",9.647503341),
                new Graph.Edge("n244","n243",8.128088671),
                new Graph.Edge("n244","n75",6.829736634),
                new Graph.Edge("n244","n245",7.820126002),
                new Graph.Edge("n81","n245",10.62927482),
                new Graph.Edge("n58","n245",17.43963044),
                new Graph.Edge("n58","n100",14.06000716),
                new Graph.Edge("n58","n246",12.7673892),
                new Graph.Edge("n96","n246",10.34047989),
                new Graph.Edge("n226","n246",30.22854436),
                new Graph.Edge("n115","n246",27.1793827),
                new Graph.Edge("n241","n247",9.491648987),
                new Graph.Edge("n247","n71",10.26130502),
                new Graph.Edge("n247","n248",10.49403301),
                new Graph.Edge("n67","n248",9.730747182),
                new Graph.Edge("n249","n248",9.230274612),
                new Graph.Edge("n248","n17",311.7409417),
                new Graph.Edge("n249","n47",10.60423427),
                new Graph.Edge("n249","n250",15.20848375),
                new Graph.Edge("n77","n250",12.45547621),
                new Graph.Edge("n48","n250",14.42541093),
                new Graph.Edge("n65","n250",9.838964578),
                new Graph.Edge("n65","n83",15.27087021),
                new Graph.Edge("n65","n251",16.36832644),
                new Graph.Edge("n65","n83",15.27087021),
                new Graph.Edge("n85","n251",90.81780427),
                new Graph.Edge("n400","n251",51.59133679),
                new Graph.Edge("n140","n251",8.63347893),
                new Graph.Edge("n252","n251",19.92550581),
                new Graph.Edge("n252","n44",10.5308949),
                new Graph.Edge("n252","n80",9.536641213),
                new Graph.Edge("n252","n253",24.39184115),
                new Graph.Edge("n102","n253",5.017877143),
                new Graph.Edge("n227","n253",48.93455869),
                new Graph.Edge("n227","n24",66.05678334),
                new Graph.Edge("n228","n24",18.92053052),
                new Graph.Edge("n228","n254",12.34391557),
                new Graph.Edge("n40","n254",12.8054483),
                new Graph.Edge("n163","n254",10.58296708),
                new Graph.Edge("n255","n254",12.37308365),
                new Graph.Edge("n255","n41",12.04423706),
                new Graph.Edge("n255","n109",12.38582309),
                new Graph.Edge("n109","n194",12.72319719),
                new Graph.Edge("n109","n195",7.595644985),
                new Graph.Edge("n197","n195",25.01276135),
                new Graph.Edge("n197","n230",32.74986379),
                new Graph.Edge("n97","n230",18.01484143),
                new Graph.Edge("n14","n230",8.58322908),
                new Graph.Edge("n256","n230",10.07876486),
                new Graph.Edge("n256","n21",7.66767681),
                new Graph.Edge("n256","n30",36.2938446),
                new Graph.Edge("n256","n14",10.71624575),
                new Graph.Edge("n30","n257",17.66732054),
                new Graph.Edge("n39","n257",8.243683194),
                new Graph.Edge("n199","n257",24.39701306),
                new Graph.Edge("n161","n257",24.3317296),
                new Graph.Edge("n162","n257",19.47098478),
                new Graph.Edge("n162","n107",22.22636368),
                new Graph.Edge("n108","n107",25.40457029),
                new Graph.Edge("n199","n202",23.48533321),
                new Graph.Edge("n198","n202",73.68655145),
                new Graph.Edge("n198","n108",28.52901426),
                new Graph.Edge("n196","n108",43.07010488),
                new Graph.Edge("n235","n116",4.26026443),
                new Graph.Edge("n166","n167",13.0300857),
                new Graph.Edge("n157","n167",6.051787537),
                new Graph.Edge("n157","n189",15.34328486),
                new Graph.Edge("n190","n189",6.241650414),
                new Graph.Edge("n117","n189",11.71462139),
                new Graph.Edge("n56","n258",3.827957379),
                new Graph.Edge("n258","n259",9.773255872),
                new Graph.Edge("n52","n259",6.215607253),
                new Graph.Edge("n1","n207",9.784775236),
                new Graph.Edge("n208","n207",80.20995109),
                new Graph.Edge("n215","n1",34.64958661),
                new Graph.Edge("n113","n1",39.06419748),
                new Graph.Edge("n113","n215",26.2613474),
                new Graph.Edge("n221","n113",46.55324045),
                new Graph.Edge("n2","n113",45.58474439),
                new Graph.Edge("n6","n62",57.28178806),
                new Graph.Edge("n216","n62",15.63506538),
                new Graph.Edge("n3","n2",24.15404196),
                new Graph.Edge("n113","n2",45.58474439),
                new Graph.Edge("n177","n3",18.04757777),
                new Graph.Edge("n4","n177",4.812529279),
                new Graph.Edge("n216","n4",72.40626389),
                new Graph.Edge("n208","n4",8.444033786),
                new Graph.Edge("n5","n4",7.462787245),
                new Graph.Edge("n222","n4",45.94892524),
                new Graph.Edge("n209","n208",29.15749135),
                new Graph.Edge("n213","n5",15.48622555),
                new Graph.Edge("n223","n5",46.58884582),
                new Graph.Edge("n222","n221",23.83498732),
                new Graph.Edge("n3","n221",37.79852739),
                new Graph.Edge("n223","n222",7.168998819),
                new Graph.Edge("n5","n223",46.58884582),
                new Graph.Edge("n62","n223",25.30967931),
                new Graph.Edge("n213","n5",15.48622555),
                new Graph.Edge("n209","n213",4.739886718),
                new Graph.Edge("n6","n213",16.02137139),
                new Graph.Edge("n210","n209",30.53967488),
                new Graph.Edge("n214","n6",13.88943973),
                new Graph.Edge("n214","n210",4.552717901),
                new Graph.Edge("n211","n210",87.78367384),
                new Graph.Edge("n7","n214",15.56285027),
                new Graph.Edge("n8","n7",27.3571853),
                new Graph.Edge("n90","n8",23.52903687),
                new Graph.Edge("n258","n8",34.11396264),
                new Graph.Edge("n145","n90",5.952694268),
                new Graph.Edge("n9","n145",19.53613545),
                new Graph.Edge("n52","n146",31.32958617),
                new Graph.Edge("n103","n259",17.12072501),
                new Graph.Edge("n9","n103",35.51576728),
                new Graph.Edge("n42","n9",20.34974784),
                new Graph.Edge("n26","n9",66.5401447),
                new Graph.Edge("n11","n10",24.90769635),
                new Graph.Edge("n45","n10",62.52368822),
                new Graph.Edge("n141","n11",16.506213),
                new Graph.Edge("n212","n11",11.62116553),
                new Graph.Edge("n211","n212",44.695743),
                new Graph.Edge("n211","n9",12.22359877),
                new Graph.Edge("n260","n216",20.84761841),
                new Graph.Edge("n260","n263",10.53731109),
                new Graph.Edge("n263","n92",7.714545686428179),
                new Graph.Edge("n149","n260",4.898830423),
                new Graph.Edge("n261","n263",10.62715503),
                new Graph.Edge("n92","n261",10.62715503),
                new Graph.Edge("n87 ","n261",6.498422818),
                new Graph.Edge("n94","n262",9.935579239),
                new Graph.Edge("n29","n262",10.17133052),
                new Graph.Edge("n264","n262",14.30307175),
                new Graph.Edge("n111","n263",40.63190525),
                new Graph.Edge("n264","n27",7.570688562),
                new Graph.Edge("n36","n264",8.204783465),
                new Graph.Edge("n16","n264",13.34781435),
                new Graph.Edge("n16","n36",5.964037785),
                new Graph.Edge("n25","n36",11.1764558),
                new Graph.Edge("n25","n16",10.6288509),
                new Graph.Edge("n265","n16",16.05424327),
                new Graph.Edge("n265","n36",21.47512739),
                new Graph.Edge("n265","n33",6.873797786),
                new Graph.Edge("n103","n265",23.59978725),
                new Graph.Edge("n178","n265",10.45876463),
                new Graph.Edge("n37","n178",12.20589196),
                new Graph.Edge("n19","n37",10.84207028),
                new Graph.Edge("n147","n37",19.91057508),
                new Graph.Edge("n266","n37",34.06095562),
                new Graph.Edge("n266","n23",12.98539971),
                new Graph.Edge("n266","n59",4.358550942),
                new Graph.Edge("n103","n178",25.10554404),
                new Graph.Edge("n267","n268",10.43979102),
                new Graph.Edge("n267","n31",8.52475846),
                new Graph.Edge("n267","n264",6.116221284),
                new Graph.Edge("n36","n267",7.133082712),
                new Graph.Edge("n16","n267",9.607252582),
                new Graph.Edge("n36","n268",13.35388937),
                new Graph.Edge("n16","n268",9.8773589),
                new Graph.Edge("n19","n268",9.087107512),
                new Graph.Edge("n25","n268",20.39641479),
                new Graph.Edge("n22","n269",10.43417858),
                new Graph.Edge("n266","n269",11.65291635),
                new Graph.Edge("n269","n270",13.48918582),
                new Graph.Edge("n266","n59",4.358550942),
                new Graph.Edge("n45","n270",29.2577682),
                new Graph.Edge("n143","n45",14.91349389),
                new Graph.Edge("n143","n11",52.71434316),
                new Graph.Edge("n141","n11",16.506213),
                new Graph.Edge("n220","n143",22.16180219),
                new Graph.Edge("n158","n220",2.774187167),
                new Graph.Edge("n141","n158",40.11966304),
                new Graph.Edge("n110","n220",9.176910072),
                new Graph.Edge("n112","n220",183.3604201),
                new Graph.Edge(" n179","n400",35.88532295),
                new Graph.Edge(" n225","n400",39.30980568),
                new Graph.Edge(" n224","n400",39.80100366),
                new Graph.Edge(" n15","n224",30.95236814),
                new Graph.Edge(" n192","n15",29.27347277),
                new Graph.Edge(" n114","n192",22.30528775),
                new Graph.Edge(" n191","n15",8.950714476),
                new Graph.Edge(" n229","n15",30.56002906),
                new Graph.Edge(" n13","n229",7.003031692),
                new Graph.Edge(" n229","n231",16.02277755),
                new Graph.Edge(" n35","n231",8.783529648),
                new Graph.Edge("n179","n224",52.81545226),
                new Graph.Edge("n225","n224",43.43928991),
                new Graph.Edge("n164","n224",22.38634903),
                new Graph.Edge("n164","n58",21.47680589),
                new Graph.Edge("n35","n58",20.90869544),
                new Graph.Edge("n232","n231",18.30416131),
                new Graph.Edge("n18","n232",7.289295568),
                new Graph.Edge("n233","n232",12.26555001),
                new Graph.Edge("n20","n233",10.1615798),
                new Graph.Edge("n226","n233",18.22175529),
                new Graph.Edge("n226","n233",18.22175529),
                new Graph.Edge("n234","n226",14.53587579),
                new Graph.Edge("n234","n235",7.441019224),
                new Graph.Edge("n235","n91",2.313662702),
                new Graph.Edge("n180","n226",14.5798274),
                new Graph.Edge("n32","n226",22.97441359),
                new Graph.Edge("n32","n180",19.34118993),
                new Graph.Edge("n32","n43",7.297327171),
                new Graph.Edge("n32","n66",9.959134033),
                new Graph.Edge("n34","n66",15.34122901),
                new Graph.Edge("n12","n66",7.316443876),
                new Graph.Edge("n43","n66",11.54413891),
                new Graph.Edge("n32","n34",15.45068727),
                new Graph.Edge("n32","n12",12.50674096),
                new Graph.Edge("n43","n66",11.54413891),
                new Graph.Edge("n43","n34",22.19958737),
                new Graph.Edge("n43","n12",17.20369052),
                new Graph.Edge("n12","n34",8.807607492),
                new Graph.Edge("n188","n66",8.936607746),
                new Graph.Edge("n34","n180",30.39029088),
                new Graph.Edge("n12","n180",22.39741668),
                new Graph.Edge("n43","n180",13.67529811),
                new Graph.Edge("n66","n180",15.15743846),
                new Graph.Edge("n236","n188",21.21611567),
                new Graph.Edge("n79","n236",153.9257521),
                new Graph.Edge("n237","n236",7.440413648),
                new Graph.Edge("n153","n237",4.202762599),
                new Graph.Edge("n118","n237",6.825776983),
                new Graph.Edge("n187","n237",15.17170094),
                new Graph.Edge("n187","n186",23.39267337),
                new Graph.Edge("n187","n238",18.08673334),
                new Graph.Edge("n38","n238",9.8151214),
                new Graph.Edge("n239","n238",30.38747365),
                new Graph.Edge("n106","n239",8.298162264),
                new Graph.Edge("n240","n239",29.26823885),
                new Graph.Edge("n240","n119",4.497953856),
                new Graph.Edge("n183","n184",29.44215047),
                new Graph.Edge("n181","n183",21.29836049),
                new Graph.Edge("n241","n225",2.50084375),
                new Graph.Edge("n241","n98",10.66904853),
                new Graph.Edge("n241","n242",20.44165268),
                new Graph.Edge("n49","n242",10.173102374695612),
                new Graph.Edge("n243","n242",13.71050859),
                new Graph.Edge("n243","n72",9.647503341),
                new Graph.Edge("n243","n244",8.128088671),
                new Graph.Edge("n75","n244",6.829736634),
                new Graph.Edge("n245","n244",7.820126002),
                new Graph.Edge("n245","n81",10.62927482),
                new Graph.Edge("n245","n58",17.43963044),
                new Graph.Edge("n100","n58",14.06000716),
                new Graph.Edge("n246","n58",12.7673892),
                new Graph.Edge("n246","n96",10.34047989),
                new Graph.Edge("n246","n226",30.22854436),
                new Graph.Edge("n246","n115",27.1793827),
                new Graph.Edge("n247","n241",9.491648987),
                new Graph.Edge("n71","n247",10.26130502),
                new Graph.Edge("n248","n247",10.49403301),
                new Graph.Edge("n248","n67",9.730747182),
                new Graph.Edge("n248","n249",9.230274612),
                new Graph.Edge("n17","n248",311.7409417),
                new Graph.Edge("n47","n249",10.60423427),
                new Graph.Edge("n250","n249",15.20848375),
                new Graph.Edge("n250","n48",14.42541093),
                new Graph.Edge("n250","n65",9.838964578),
                new Graph.Edge("n83","n65",15.27087021),
                new Graph.Edge("n251","n65",16.36832644),
                new Graph.Edge("n83","n65",15.27087021),
                new Graph.Edge("n251","n85",90.81780427),
                new Graph.Edge("n251","n400",51.59133679),
                new Graph.Edge("n251","n140",8.63347893),
                new Graph.Edge("n251","n252",19.92550581),
                new Graph.Edge("n44","n252",10.5308949),
                new Graph.Edge("n80","n252",9.536641213),
                new Graph.Edge("n253","n252",24.39184115),
                new Graph.Edge("n253","n102",5.017877143),
                new Graph.Edge("n253","n227",48.93455869),
                new Graph.Edge("n24","n227",66.05678334),
                new Graph.Edge("n24","n228",18.92053052),
                new Graph.Edge("n254","n228",12.34391557),
                new Graph.Edge("n254","n40",12.8054483),
                new Graph.Edge("n254","n163",10.58296708),
                new Graph.Edge("n254","n255",12.37308365),
                new Graph.Edge("n41","n255",12.04423706),
                new Graph.Edge("n109","n255",12.38582309),
                new Graph.Edge("n194","n109",12.72319719),
                new Graph.Edge("n195","n109",7.595644985),
                new Graph.Edge("n195","n197",25.01276135),
                new Graph.Edge("n230","n197",32.74986379),
                new Graph.Edge("n230","n97",18.01484143),
                new Graph.Edge("n230","n14",8.58322908),
                new Graph.Edge("n230","n256",10.07876486),
                new Graph.Edge("n21","n256",7.66767681),
                new Graph.Edge("n30","n256",36.2938446),
                new Graph.Edge("n14","n256",10.71624575),
                new Graph.Edge("n257","n30",17.66732054),
                new Graph.Edge("n257","n39",8.243683194),
                new Graph.Edge("n257","n199",24.39701306),
                new Graph.Edge("n257","n161",24.3317296),
                new Graph.Edge("n257","n162",19.47098478),
                new Graph.Edge("n107","n162",22.22636368),
                new Graph.Edge("n202","n199",23.48533321),
                new Graph.Edge("n202","n198",73.68655145),
                new Graph.Edge("n108","n198",28.52901426),
                new Graph.Edge("n108","n196",43.07010488),
                new Graph.Edge("n116","n235",4.26026443),
                new Graph.Edge("n167","n166",13.0300857),
                new Graph.Edge("n167","n157",6.051787537),
                new Graph.Edge("n189","n157",15.34328486),
                new Graph.Edge("n189","n190",6.241650414),
                new Graph.Edge("n189","n117",11.71462139),
                new Graph.Edge("n258","n56",3.827957379),
                new Graph.Edge("n259","n258",9.773255872),
                new Graph.Edge("n259","n52",6.215607253),
                new Graph.Edge("n216","n400",88.1633097039766),
                new Graph.Edge("n185","n184",18.82024178738099),
                new Graph.Edge("n184","n185",18.82024178738099),
                new Graph.Edge("n185","n240",9.329329317195107),
                new Graph.Edge("n240","n185",9.329329317195107),
                new Graph.Edge(currentLocation,getNodes(),distanceToNode),
                new Graph.Edge(getNodes(),currentLocation,distanceToNode)};

        GRAPH = newGraph;

    }
    node currentNode;
    String currentLocation;
    String end;
    String neighbour;
    double distanceToNode;

    /**
     * The node class is an object that defines the node
     */
    class node {
        String name;
        double lat;
        double lon;
        int where;

        node(String name, double lat, double lon, int where) {
            this.name = name;
            this.lat = lat;
            this.lon = lon;
            this.where = where;
        }

    }


    public static ArrayList<String> passLocations(ArrayList<String> dest, Double[] source) {
        ArrayList<String> sendToMap= new ArrayList<String>();
        String endNode = "";
        String finalNode = "";

        ArrayList<String> selectLocation = dest;

        //The Ray Casting Algorithm is run here
        double [] pnt = {source[0], source[1]};
        int  where = 0;
        for (double[][] shape : shapes) {

            if (contains(shape, pnt) == false){
                where++;
            }else{
                break;
            }

        }

        if(where >= 2){
            where = 0;
        }

        //Ad in the new node
        Dijkstra newTep = new Dijkstra(new Dijkstra().new node("currentLocation", source[0], source[1], where));

        //Finds the closest destiation if there are multiple
        endNode = selectLocation.get(0);
        finalNode = endNode;
        newTep.end = endNode;
        Graph G = new Graph(newTep.GRAPH);
        if (selectLocation.size() > 1) {
            G.dijkstra(newTep.currentNode.name);
            Double nearest = G.getDist(newTep.end);
            Double check = 0.0;
            for (int i = 0; i < selectLocation.size(); i++) {
                endNode = selectLocation.get(i);
                String nearestNode;
                newTep.end = endNode;
                G.dijkstra(newTep.currentNode.name);
                check = G.getDist(newTep.end);
                if (check < nearest) {
                    nearest = check;
                    nearestNode = endNode;
                    finalNode = nearestNode;
                }
            }
        }


        newTep.getNodes();


        //add the new node to graph and run Dijkstra's
        newTep.end = finalNode;

        G.dijkstra(newTep.currentNode.name);
        sendToMap = G.printPath(newTep.end);

        return (sendToMap);
    }

    //zone 1
    final static double[][] beforeSecurity = {{-33.9376489,151.164513},
            {-33.9377152,151.1645693},
            {-33.9376284,151.1647229},
            {-33.937773,151.1648402},
            {-33.9377263,151.1649241},
            {-33.9381196,151.1652392},
            {-33.9380584,151.1653411},
            {-33.9380039,151.1652942},
            {-33.9379572,151.1654203},
            {-33.9379138,151.1654048},
            {-33.9379093,151.1654565},
            {-33.9379972,151.1654913},
            {-33.9378142,151.1658521},
            {-33.9377207,151.1660831},
            {-33.9376729,151.1661797},
            {-33.9376439,151.1662816},
            {-33.9371908,151.1665056},
            {-33.9372072,151.1665548},
            {-33.9369777,151.1666662},
            {-33.9368721,151.1663597},
            {-33.936859,151.1663657},
            {-33.9368915,151.1664563},
            {-33.9368203,151.1664891},
            {-33.9367816,151.1663792},
            {-33.936531,151.1665056},
            {-33.9365346,151.1665143},
            {-33.9365152,151.1665243},
            {-33.9365113,151.1665153},
            {-33.9361886,151.1666752},
            {-33.9362682,151.1669035},
            {-33.9379972,151.1654913},
            {-33.9378142,151.1658521},
            {-33.9377207,151.1660831},
            {-33.9376729,151.1661797},
            {-33.9376439,151.1662816},
            {-33.9371908,151.1665056},
            {-33.9372072,151.1665548},
            {-33.9369777,151.1666662},
            {-33.9368721,151.1663597},
            {-33.936859,151.1663657},
            {-33.9368915,151.1664563},
            {-33.9368203,151.1664891},
            {-33.9359132,151.1670752},
            {-33.9358081,151.1667741},
            {-33.9354147,151.1669659},
            {-33.9352239,151.166412},
            {-33.9368901,151.1655959},
            {-33.9371772,151.1653706}};

    //zone 2
    final static double[][] afterSecurity = {{-33.9381196,151.1652392},
            {-33.9380584,151.1653411},
            {-33.9380039,151.1652942},
            {-33.9379572,151.1654203},
            {-33.9379138,151.1654048},
            {-33.9379093,151.1654565},
            {-33.9367816,151.1663792},
            {-33.936531,151.1665056},
            {-33.9365346,151.1665143},
            {-33.9365152,151.1665243},
            {-33.9365113,151.1665153},
            {-33.9361886,151.1666752},
            {-33.9362682,151.1669035},
            {-33.9359132,151.1670752},
            {-33.9371772,151.1653706},
            {-33.9360323,151.1670189},
            {-33.9361224,151.167275},
            {-33.9370281,151.1668244},
            {-33.9352445,151.1673743},
            {-33.9341919,151.1672723},
            {-33.9341418,151.1677471},
            {-33.9351243,151.1681655},
            {-33.9361257,151.1694691},
            {-33.936642,151.1705339},
            {-33.9369513,151.1701101},
            {-33.9369836,151.169178},
            {-33.937393,151.1692639},
            {-33.9393791,151.170086},
            {-33.9397062,151.16871},
            {-33.9372117,151.1678597},
            {-33.9373051,151.1671127},
            {-33.937442,151.166461},
            {-33.9377558,151.1663054},
            {-33.9379482,151.1659648},
            {-33.9391721,151.1663979},
            {-33.9391432,151.1666957},
            {-33.9393813,151.1667708},
            {-33.9398686,151.1665857},
            {-33.9401223,151.165781},
            {-33.9403715,151.1645472},
            {-33.940445,151.1635467},
            {-33.9397552,151.1635146},
            {-33.9390787,151.1645874},
            {-33.9382553,151.1650407}};

    //iniialise zones
    final static double[][][] shapes = {beforeSecurity, afterSecurity};


    //check for the number of intersects
    static boolean intersects(double[] shape, double[] shape2, double[] P) {
        if (shape[1] > shape2[1])
            return intersects(shape2, shape, P);

        if (P[1] == shape[1] || P[1] == shape2[1])
            P[1] += 0.0000001;

        if (P[1] > shape2[1] || P[1] < shape[1] || P[0] >= max(shape[0], shape2[0]))
            return false;

        if (P[0] < min(shape[0], shape2[0]))
            return true;

        double red = (P[1] - shape[1]) / (double) (P[0] - shape[0]);
        double blue = (shape2[1] - shape[1]) / (double) (shape2[0] - shape[0]);
        return red >= blue;
    }

    //check if a node is in a zone
    static boolean contains(double[][] shape, double [] pnt) {
        boolean inside = false;
        double len = shape.length;
        for (int i = 0; i < len; i=i+1) {
            if (intersects(shape[i], shape[(int) ((i + 1) % len)], pnt))
                inside = !inside;
        }
        return inside;
    }

    public String getNodes() {
        /**
         * current nodes
         */
        node n1 = new Dijkstra().new node("1", -33.935405, 151.16643, 0);
        node n2 = new Dijkstra().new node("2", -33.935696, 151.166259, 0);
        node n3 = new Dijkstra().new node("3", -33.935895, 151.166154, 0);
        node n4 = new Dijkstra().new node("4", -33.936078, 151.166061, 0);
        node n5 = new Dijkstra().new node("5", -33.93614, 151.16603, 0);
        node n6 = new Dijkstra().new node("6", -33.936404, 151.16591, 0);
        node n7 = new Dijkstra().new node("7", -33.936646, 151.165788, 0);
        node n8 = new Dijkstra().new node("8", -33.936871, 151.165668, 0);
        node n9 = new Dijkstra().new node("9", -33.937265, 151.165436, 0);
        node n10 = new Dijkstra().new node("10", -33.937403, 151.165188, 0);
        node n11 = new Dijkstra().new node("11", -33.937523, 151.16496, 0);
        node n12 = new Dijkstra().new node("12", -33.937247, 151.168735, 1);
        node n13 = new Dijkstra().new node("13", -33.936529, 151.168207, 1);
        node n14 = new Dijkstra().new node("14", -33.938909, 151.16547, 1);
        node n15 = new Dijkstra().new node("15", -33.9364, 151.167881, 1);
        node n16 = new Dijkstra().new node("16", -33.937181, 151.166203, 0);
        node n17 = new Dijkstra().new node("17", -33.939397, 151.165595, 1);
        node n18 = new Dijkstra().new node("18", -33.936699, 151.168427, 1);
        node n19 = new Dijkstra().new node("19", -33.9373194, 151.1663006, 0);
        node n20 = new Dijkstra().new node("20", -33.93677, 151.168564, 1);
        node n21 = new Dijkstra().new node("21", -33.938968, 151.165387, 1);
        node n22 = new Dijkstra().new node("22", -33.937682, 151.165872, 0);
        node n23 = new Dijkstra().new node("23", -33.937647, 151.166007, 0);
        node n24 = new Dijkstra().new node("24", -33.9379978, 151.1656935, 1);
        node n25 = new Dijkstra().new node("25", -33.937135, 151.166102, 0);
        node n26 = new Dijkstra().new node("26", -33.937791, 151.16578, 0);
        node n27 = new Dijkstra().new node("27", -33.937096, 151.16638, 0);
        node n29 = new Dijkstra().new node("29", -33.936955, 151.166352, 0);
        node n30 = new Dijkstra().new node("30", -33.939311, 151.165321, 1);
        node n31 = new Dijkstra().new node("31", -33.937169, 151.166386, 0);
        node n32 = new Dijkstra().new node("32", -33.9371391, 151.1687733, 1);
        node n33 = new Dijkstra().new node("33", -33.937255, 151.166008, 0);
        node n34 = new Dijkstra().new node("34", -33.937271, 151.168826, 1);
        node n35 = new Dijkstra().new node("35", -33.936625, 151.168291, 1);
        node n36 = new Dijkstra().new node("36", -33.93713, 151.166223, 0);
        node n37 = new Dijkstra().new node("37", -33.937337, 151.166185, 0);
        node n38 = new Dijkstra().new node("38", -33.937774, 151.168882, 1);
        node n39 = new Dijkstra().new node("39", -33.939458, 151.165151, 1);
        node n40 = new Dijkstra().new node("40", -33.938269, 151.165457, 1);
        node n41 = new Dijkstra().new node("41", -33.938279, 151.165727, 0);
        node n42 = new Dijkstra().new node("42", -33.937293, 151.165218, 1);
        node n43 = new Dijkstra().new node("43", -33.937093, 151.168717, 1);
        node n44 = new Dijkstra().new node("44", -33.937139, 151.166673, 1);
        node n45 = new Dijkstra().new node("45", -33.937896, 151.165514, 1);
        node n400 = new Dijkstra().new node("400", -33.936788, 151.167383, 0);
        node n47 = new Dijkstra().new node("47", -33.937161, 151.167394, 1);
        node n48 = new Dijkstra().new node("48", -33.937163, 151.16715, 1);
        node n49 = new Dijkstra().new node("49", -33.937118, 151.167907, 1);
        node n52 = new Dijkstra().new node("52", -33.937149, 151.165951, 0);
        node n54 = new Dijkstra().new node("54", -33.936308, 151.167818, 1);
        node n56 = new Dijkstra().new node("56", -33.937079, 151.165994, 0);
        node n58 = new Dijkstra().new node("58", -33.936807, 151.168234, 1);
        node n59 = new Dijkstra().new node("59", -33.937528, 151.165887, 0);
        node n61 = new Dijkstra().new node("61", -33.939332, 151.165999, 1);
        node n62 = new Dijkstra().new node("62", -33.93652, 151.166515, 0);
        node n63 = new Dijkstra().new node("63", -33.939332, 151.165999, 1);
        node n64 = new Dijkstra().new node("64", -33.939254, 151.165881, 1);
        node n65 = new Dijkstra().new node("65", -33.93702, 151.167119, 1);
        node n66 = new Dijkstra().new node("66", -33.937194, 151.168688, 1);
        node n67 = new Dijkstra().new node("67", -33.937159, 151.167495, 1);
        node n71 = new Dijkstra().new node("71", -33.937159, 151.167606, 1);
        node n72 = new Dijkstra().new node("72", -33.937075, 151.168018, 1);
        node n73 = new Dijkstra().new node("73", -33.936826, 151.166427, 0);
        node n75 = new Dijkstra().new node("75", -33.937007, 151.168063, 1);
        node n77 = new Dijkstra().new node("77", -33.937159, 151.167229, 1);
        node n79 = new Dijkstra().new node("79", -33.937216, 151.167027, 1);
        node n80 = new Dijkstra().new node("80", -33.937139, 151.166685, 1);
        node n81 = new Dijkstra().new node("81", -33.936984, 151.168151, 1);
        node n83 = new Dijkstra().new node("83", -33.937148, 151.167059, 1);
        node n85 = new Dijkstra().new node("85", -33.937296, 151.166016, 1);
        node n87 = new Dijkstra().new node("87", -33.936889, 151.166196, 0);
        node n89 = new Dijkstra().new node("89", -33.936275, 151.165972, 0);
        node n90 = new Dijkstra().new node("90", -33.937069, 151.165578, 0);
        node n91 = new Dijkstra().new node("91", -33.936858, 151.168818, 1);
        node n92 = new Dijkstra().new node("92", -33.936827, 151.166216, 0);
        node n94 = new Dijkstra().new node("94", -33.936945, 151.16614, 0);
        node n96 = new Dijkstra().new node("96", -33.936995, 151.168337, 1);
        node n97 = new Dijkstra().new node("97", -33.938969, 151.165741, 1);
        node n98 = new Dijkstra().new node("98", -33.937144, 151.167741, 1);
        node n100 = new Dijkstra().new node("100", -33.936932, 151.168211, 1);
        node n102 = new Dijkstra().new node("102", -33.937334, 151.166571, 1);
        node n103 = new Dijkstra().new node("103", -33.937265, 151.165821, 0);
        node n106 = new Dijkstra().new node("106", -33.938011, 151.16895, 1);
        node n107 = new Dijkstra().new node("107", -33.93957, 151.165375, 1);
        node n108 = new Dijkstra().new node("108", -33.939514, 151.165642, 1);
        node n109 = new Dijkstra().new node("109", -33.9384044, 151.1656692, 1);
        node n110 = new Dijkstra().new node("110", -33.938033, 151.165207, 0);
        node n111 = new Dijkstra().new node("111", -33.937006, 151.1666, 0);
        node n112 = new Dijkstra().new node("112", -33.937067, 151.166817, 1);
        node n113 = new Dijkstra().new node("113", -33.935637, 151.166748, 0);
        node n114 = new Dijkstra().new node("114", -33.936192, 151.167624, 1);
        node n115 = new Dijkstra().new node("115", -33.937135, 151.1682, 1);
        node n116 = new Dijkstra().new node("116", -33.9368747, 151.1688705, 1);
        node n117 = new Dijkstra().new node("117", -33.936856, 151.169325, 1);
        node n118 = new Dijkstra().new node("118", -33.937471, 151.168768, 1);
        node n119 = new Dijkstra().new node("119", -33.938293, 151.168894, 1);
        node n140 = new Dijkstra().new node("140", -33.937037, 151.166924, 1);
        node n141 = new Dijkstra().new node("141", -33.937657, 151.164883, 0);
        node n143 = new Dijkstra().new node("143", -33.937864, 151.165357, 0);
        node n145 = new Dijkstra().new node("145", -33.937118, 151.165552, 0);
        node n146 = new Dijkstra().new node("146", -33.937228, 151.165625, 0);
        node n147 = new Dijkstra().new node("147", -33.937516, 151.166179, 0);
        node n149 = new Dijkstra().new node("149", -33.936809, 151.166275, 0);
        node n153 = new Dijkstra().new node("153", -33.937452, 151.168729, 1);
        node n157 = new Dijkstra().new node("157", -33.936788, 151.169225, 1);
        node n158 = new Dijkstra().new node("158", -33.937959, 151.165121, 0);
        node n161 = new Dijkstra().new node("161", -33.939334, 151.16546, 1);
        node n162 = new Dijkstra().new node("162", -33.939375, 151.165428, 1);
        node n163 = new Dijkstra().new node("163", -33.938178, 151.16568, 1);
        node n164 = new Dijkstra().new node("164", -33.9367325, 151.1680192, 1);
        node n166 = new Dijkstra().new node("166", -33.936835, 151.169027, 1);
        node n167 = new Dijkstra().new node("167", -33.93681, 151.169165, 1);
        node n177 = new Dijkstra().new node("177", -33.936035, 151.166055, 0);
        node n178 = new Dijkstra().new node("178", -33.937373, 151.16606, 0);
        node n179 = new Dijkstra().new node("179", -33.936481, 151.167263, 1);
        node n180 = new Dijkstra().new node("180", -33.9371, 151.168569, 1);
        node n181 = new Dijkstra().new node("181", -33.938969, 151.169169, 1);
        node n183 = new Dijkstra().new node("183", -33.938787, 151.169097, 1);
        node n184 = new Dijkstra().new node("184", -33.938532, 151.169011, 1);
        node n185 = new Dijkstra().new node("185", -33.938369, 151.168956, 1);
        node n186 = new Dijkstra().new node("186", -33.937816, 151.168779, 1);
        node n187 = new Dijkstra().new node("187", -33.937611, 151.168722, 1);
        node n188 = new Dijkstra().new node("188", -33.937231, 151.168602, 1);
        node n189 = new Dijkstra().new node("189", -33.936765, 151.169389, 1);
        node n190 = new Dijkstra().new node("190", -33.9368019, 151.16944, 1);
        node n191 = new Dijkstra().new node("191", -33.9363505, 151.1679575, 1);
        node n192 = new Dijkstra().new node("192", -33.9361375, 151.1678567, 1);
        node n194 = new Dijkstra().new node("194", -33.9384347, 151.1655362, 1);
        node n195 = new Dijkstra().new node("195", -33.9384716, 151.1656544, 1);
        node n196 = new Dijkstra().new node("196", -33.9391715, 151.1658601, 1);
        node n197 = new Dijkstra().new node("197", -33.9386916, 151.165711, 1);
        node n198 = new Dijkstra().new node("198", -33.9394054, 151.1659222, 1);
        node n199 = new Dijkstra().new node("199", -33.9395217, 151.1649883, 1);
        node n202 = new Dijkstra().new node("202", -33.9396574, 151.1651834, 1);
        node n207 = new Dijkstra().new node("207", -33.93534, 151.1663585, 0);
        node n208 = new Dijkstra().new node("208", -33.9360072, 151.1660279, 0);
        node n209 = new Dijkstra().new node("209", -33.9362499, 151.1659082, 0);
        node n210 = new Dijkstra().new node("210", -33.9365043, 151.1657834, 0);
        node n211 = new Dijkstra().new node("211", -33.9371986, 151.1653304, 0);
        node n212 = new Dijkstra().new node("212", -33.937422, 151.1649276, 0);
        node n213 = new Dijkstra().new node("213", -33.9362651, 151.1659562, 0);
        node n214 = new Dijkstra().new node("214", -33.9365109, 151.1658321, 0);
        node n215 = new Dijkstra().new node("215", -33.9354057, 151.1668056, 0);
        node n216 = new Dijkstra().new node("216", -33.9366471, 151.1664425, 0);
        node n220 = new Dijkstra().new node("220", -33.9379653, 151.1651501, 0);
        node n221 = new Dijkstra().new node("221", -33.9360171, 151.1665364, 0);
        node n222 = new Dijkstra().new node("222", -33.9362312, 151.1665236, 0);
        node n223 = new Dijkstra().new node("223", -33.9362927, 151.1665003, 0);
        node n224 = new Dijkstra().new node("224", -33.9366678, 151.1677894, 1);
        node n225 = new Dijkstra().new node("225", -33.9370465, 151.1676737, 1);
        node n226 = new Dijkstra().new node("226", -33.936976, 151.1686204, 1);
        node n227 = new Dijkstra().new node("227", -33.9376792, 151.1662979, 1);
        node n228 = new Dijkstra().new node("228", -33.9380953, 151.1655254, 1);
        node n229 = new Dijkstra().new node("229", -33.9365657, 151.1681453, 1);
        node n230 = new Dijkstra().new node("230", -33.9389527, 151.1655467, 1);
        node n231 = new Dijkstra().new node("231", -33.936689, 151.1682352, 1);
        node n232 = new Dijkstra().new node("232", -33.9367634, 151.1684122, 1);
        node n233 = new Dijkstra().new node("233", -33.9368452, 151.1685014, 1);
        node n234 = new Dijkstra().new node("234", -33.9368993, 151.168748, 1);
        node n235 = new Dijkstra().new node("235", -33.9368781, 151.1688245, 1);
        node n236 = new Dijkstra().new node("236", -33.9374108, 151.168679, 1);
        node n237 = new Dijkstra().new node("237", -33.9374765, 151.1686943, 1);
        node n238 = new Dijkstra().new node("238", -33.9377674, 151.1687759, 1);
        node n239 = new Dijkstra().new node("239", -33.9380309, 151.1688633, 1);
        node n240 = new Dijkstra().new node("240", -33.9382859, 151.168942, 1);
        node n241 = new Dijkstra().new node("241", -33.9370546, 151.167699, 1);
        node n242 = new Dijkstra().new node("242", -33.9370345, 151.1678619, 1);
        node n243 = new Dijkstra().new node("243", -33.9370028, 151.16796, 1);
        node n244 = new Dijkstra().new node("244", -33.936954, 151.1680256, 1);
        node n245 = new Dijkstra().new node("245", -33.9369049, 151.1680863, 1);
        node n246 = new Dijkstra().new node("246", -33.9369062, 151.1683037, 1);
        node n247 = new Dijkstra().new node("247", -33.937067, 151.1675972, 1);
        node n248 = new Dijkstra().new node("248", -33.937072, 151.1674836, 1);
        node n249 = new Dijkstra().new node("249", -33.937066, 151.1673838, 1);
        node n250 = new Dijkstra().new node("250", -33.9370472, 151.1672205, 1);
        node n251 = new Dijkstra().new node("251", -33.9371033, 151.1669727, 1);
        node n252 = new Dijkstra().new node("252", -33.9371786, 151.1667767, 1);
        node n253 = new Dijkstra().new node("253", -33.9372922, 151.1665505, 1);
        node n254 = new Dijkstra().new node("254", -33.9382004, 151.1655685, 1);
        node n255 = new Dijkstra().new node("255", -33.9383083, 151.1656013, 1);
        node n256 = new Dijkstra().new node("256", -33.9390048, 151.1654573, 1);
        node n257 = new Dijkstra().new node("257", -33.9394551, 151.1652403, 1);
        node n258 = new Dijkstra().new node("258", -33.937057, 151.1659621, 0);
        node n259 = new Dijkstra().new node("259", -33.9371226, 151.1658916, 0);
        node n260 = new Dijkstra().new node("260", -33.9368088, 151.1663281, 0);
        node n261 = new Dijkstra().new node("261", -33.9369158, 151.1662586, 0);
        node n262 = new Dijkstra().new node("262", -33.9369703, 151.1662433, 0);
        node n263 = new Dijkstra().new node("263", -33.936859, 151.1662902, 0);
        node n264 = new Dijkstra().new node("264", -33.9370906, 151.1662982, 0);
        node n265 = new Dijkstra().new node("265", -33.9372799, 151.1660762, 0);
        node n266 = new Dijkstra().new node("266", -33.9375531, 151.1659233, 0);
        node n267 = new Dijkstra().new node("267", -33.9371456, 151.166298, 0);
        node n268 = new Dijkstra().new node("268", -33.9372388, 151.1662843, 0);
        node n269 = new Dijkstra().new node("269", -33.9376025, 151.1658119, 0);
        node n270 = new Dijkstra().new node("270", -33.9376877, 151.1657078, 0);


        ArrayList<node> al = new ArrayList<node>();
        al.add(n1);
        al.add(n2);
        al.add(n3);
        al.add(n4);
        al.add(n5);
        al.add(n6);
        al.add(n7);
        al.add(n8);
        al.add(n9);
        al.add(n10);
        al.add(n11);
        al.add(n12);
        al.add(n13);
        al.add(n14);
        al.add(n15);
        al.add(n16);
        al.add(n17);
        al.add(n18);
        al.add(n19);
        al.add(n20);
        al.add(n21);
        al.add(n22);
        al.add(n23);
        al.add(n24);
        al.add(n25);
        al.add(n26);
        al.add(n27);
        al.add(n29);
        al.add(n30);
        al.add(n31);
        al.add(n32);
        al.add(n33);
        al.add(n34);
        al.add(n35);
        al.add(n36);
        al.add(n37);
        al.add(n38);
        al.add(n39);
        al.add(n40);
        al.add(n41);
        al.add(n42);
        al.add(n43);
        al.add(n44);
        al.add(n45);
        al.add(n400);
        al.add(n47);
        al.add(n48);
        al.add(n49);
        al.add(n52);
        al.add(n54);
        al.add(n56);
        al.add(n58);
        al.add(n59);
        al.add(n61);
        al.add(n62);
        al.add(n63);
        al.add(n64);
        al.add(n65);
        al.add(n66);
        al.add(n67);
        al.add(n71);
        al.add(n72);
        al.add(n73);
        al.add(n75);
        al.add(n77);
        al.add(n79);
        al.add(n80);
        al.add(n81);
        al.add(n83);
        al.add(n85);
        al.add(n87);
        al.add(n89);
        al.add(n90);
        al.add(n91);
        al.add(n92);
        al.add(n94);
        al.add(n96);
        al.add(n97);
        al.add(n98);
        al.add(n100);
        al.add(n102);
        al.add(n103);
        al.add(n106);
        al.add(n107);
        al.add(n108);
        al.add(n109);
        al.add(n110);
        al.add(n111);
        al.add(n112);
        al.add(n113);
        al.add(n114);
        al.add(n115);
        al.add(n116);
        al.add(n117);
        al.add(n118);
        al.add(n119);
        al.add(n140);
        al.add(n141);
        al.add(n143);
        al.add(n145);
        al.add(n146);
        al.add(n147);
        al.add(n149);
        al.add(n153);
        al.add(n157);
        al.add(n158);
        al.add(n161);
        al.add(n162);
        al.add(n163);
        al.add(n164);
        al.add(n166);
        al.add(n167);
        al.add(n177);
        al.add(n178);
        al.add(n179);
        al.add(n180);
        al.add(n181);
        al.add(n183);
        al.add(n184);
        al.add(n185);
        al.add(n186);
        al.add(n187);
        al.add(n188);
        al.add(n189);
        al.add(n190);
        al.add(n191);
        al.add(n192);
        al.add(n194);
        al.add(n195);
        al.add(n196);
        al.add(n197);
        al.add(n198);
        al.add(n199);
        al.add(n202);
        al.add(n207);
        al.add(n208);
        al.add(n209);
        al.add(n210);
        al.add(n211);
        al.add(n212);
        al.add(n213);
        al.add(n214);
        al.add(n215);
        al.add(n216);
        al.add(n220);
        al.add(n221);
        al.add(n222);
        al.add(n223);
        al.add(n224);
        al.add(n225);
        al.add(n226);
        al.add(n227);
        al.add(n228);
        al.add(n229);
        al.add(n230);
        al.add(n231);
        al.add(n232);
        al.add(n233);
        al.add(n234);
        al.add(n235);
        al.add(n236);
        al.add(n237);
        al.add(n238);
        al.add(n239);
        al.add(n240);
        al.add(n241);
        al.add(n242);
        al.add(n243);
        al.add(n244);
        al.add(n245);
        al.add(n246);
        al.add(n247);
        al.add(n248);
        al.add(n249);
        al.add(n250);
        al.add(n251);
        al.add(n252);
        al.add(n253);
        al.add(n254);
        al.add(n255);
        al.add(n256);
        al.add(n257);
        al.add(n258);
        al.add(n259);
        al.add(n260);
        al.add(n261);
        al.add(n262);
        al.add(n263);
        al.add(n264);
        al.add(n265);
        al.add(n266);
        al.add(n267);
        al.add(n268);
        al.add(n269);
        al.add(n270);



         //insert new node into the correct part of the graph

        node shortestNode;
        if (currentNode.where == 1){
            shortestNode = n400;
        }else{
            shortestNode = n1;
        }

        double distanceToNode = distance(currentNode, shortestNode);
        double tempNode;
        Iterator itr = al.iterator();

        //traverse elements of ArrayList object
        while (itr.hasNext()) {
            node nd = (node) itr.next();
            if (nd.where == currentNode.where) {
                tempNode = distance(currentNode, nd);
                if (tempNode < distanceToNode) {
                    distanceToNode = tempNode;
                    shortestNode = nd;
                }
            }

        }
        neighbour = "n" + shortestNode.name;
        return neighbour;
    }

    private static double distance(node n1, node n2) {
        double dist = 0;
        double lat1 = n1.lat;
        double lon1 = n1.lon;
        double lat2 = n2.lat;
        double lon2 = n2.lon;

        double theta = lon1 - lon2;
        dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515 * 1.609344 * 1000;

        return dist;

    }


    //converts decimals to radians
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    //convert radians to decimals
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }



    /**
     * Initialise edges
     */

    public Graph.Edge[] GRAPH;

}
class Graph {
    private final Map<String, Vertex> graph; // mapping of vertex names to Vertex objects, built from a set of Edges

    public Graph(Map<String, Vertex> newGraph)
    {
        graph = newGraph;
    }
    /**
     * One edge of the graph (only used by Graph constructor)
     */
    public static class Edge {
        public final String v1, v2;
        public final double dist;

        public Edge(String v1, String v2, double d) {
            this.v1 = v1;
            this.v2 = v2;
            this.dist = d;
        }
    }

    /**
     * One vertex of the graph, complete with mappings to neighbouring vertices
     */
    static ArrayList<String> currentPathNode = new ArrayList<String>();

    public static class Vertex implements Comparable<Vertex> {
        public final String name;
        public double dist = Integer.MAX_VALUE; // MAX_VALUE assumed to be infinity
        public Vertex previous = null;
        public final Map<Vertex, Double> neighbours = new HashMap<>();

        public Vertex(String name) {
            this.name = name;
        }

        public ArrayList <String> printPath() {
            currentPathNode.clear();
            if (this == this.previous) {
                currentPathNode.add(this.name);
            } else if (this.previous == null) {
                System.out.printf("%s(unreached)", this.name);
            } else {
                this.previous.printPath();
                currentPathNode.add(this.name);
            }
            return currentPathNode;
        }


        //gets the distance to the node
        private Double getDist() {
            if (this == this.previous) {

            } else if (this.previous == null) {

            } else {

                this.previous.getDist();

            }

            return this.dist;
        }

        public int compareTo(Vertex other) {
            if (dist == other.dist)
                return name.compareTo(other.name);

            return Double.compare(dist, other.dist);
        }

        @Override
        public String toString() {
            return "(" + name + ", " + dist + ")";
        }
    }

    /**
     * Builds a graph from a set of edges
     */
    public Graph(Edge[] edges) {
        graph = new HashMap<>(edges.length);

        //one pass to find all vertices
        for (Edge e : edges) {
            if (!graph.containsKey(e.v1)) graph.put(e.v1, new Vertex(e.v1));
            if (!graph.containsKey(e.v2)) graph.put(e.v2, new Vertex(e.v2));
        }

        //another pass to set neighbouring vertices
        for (Edge e : edges) {
            graph.get(e.v1).neighbours.put(graph.get(e.v2), e.dist);
            //graph.get(e.v2).neighbours.put(graph.get(e.v1), e.dist); // also do this for an undirected graph
        }
    }
    /**
     * Runs dijkstra using a specified source vertex
     */
    public void dijkstra(String startName) {
        if (!graph.containsKey(startName)) {
            System.err.printf("Graph doesn't contain start vertex \"%s\"\n", startName);
            return;
        }
        final Vertex source = graph.get(startName);
        NavigableSet<Vertex> q = new TreeSet<>();

        // set-up vertices
        for (Vertex v : graph.values()) {
            v.previous = v == source ? source : null;
            v.dist = v == source ? 0 : Integer.MAX_VALUE;
            q.add(v);
        }
        dijkstra(q);
    }

    /**
     * Implementation of dijkstra's algorithm using a binary heap.
     */
    private void dijkstra(final NavigableSet<Vertex> q) {
        //Double totalDist = 0.0;
        Vertex u, v;
        while (!q.isEmpty()) {

            u = q.pollFirst(); // vertex with shortest distance (first iteration will return source)
            if (u.dist == Double.MAX_VALUE)
                break; // we can ignore u (and any other remaining vertices) since they are unreachable
                //totalDist = totalDist + u.dist;

            //look at distances to each neighbour
            for (Map.Entry<Vertex, Double> a : u.neighbours.entrySet()) {
                v = a.getKey(); //the neighbour in this iteration
                final double alternateDist = u.dist + a.getValue();
                //totalDist = totalDist + alternateDist;
                if (alternateDist < v.dist) { // shorter path to neighbour found
                    //totalDist = totalDist - v.dist;
                    q.remove(v);
                    v.dist = alternateDist;
                    //totalDist = totalDist + v.dist;
                    v.previous = u;
                    q.add(v);
                }
            }
        }
    }

    /**
     * Prints a path from the source to the specified vertex
     */
    public ArrayList <String> printPath(String endName) {
        ArrayList<String> pathArray = new ArrayList<String>();
        if (!graph.containsKey(endName)) {
            System.err.printf("Graph doesn't contain end vertex \"%s\"\n", endName);
            return null;
        }

        pathArray.addAll(graph.get(endName).printPath());

        return pathArray;
    }

    public Double getDist(String endName) {
        if (!graph.containsKey(endName)) {
            System.err.printf("Graph doesn't contain end vertex \"%s\"\n", endName);
            return null;
        }
        return graph.get(endName).getDist();
    }


    /**
     * Prints the path from the source to every vertex (output order is not guaranteed)
     */
    public void printAllPaths() {
        for (Vertex v : graph.values()) {
            v.printPath();
        }
    }

}