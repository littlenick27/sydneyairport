package com.nick.sydneyairport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import java.util.ArrayList;


public class mapsActivity extends AppCompatActivity {

    mapsActivityLayout mapsActivityLayoutView;
    public ArrayList<ArrayList<Double>> route;

    public Double[] currentLocation = new Double[2];
    public Place end;

    //gets a set of coordinates from a list of nodes
    private ArrayList<ArrayList<Double>> getCoords(ArrayList<String> path, Double[] currentLocation) {
        ArrayList<Double> currentLoc = new ArrayList<Double>();
        currentLoc.add(currentLocation[0]);
        currentLoc.add(currentLocation[1]);
        ArrayList<ArrayList<Double>> printPath = new ArrayList<ArrayList<Double>>();
        printPath.add(currentLoc);
        for (int i = 0; i < path.size(); i++) {

            ArrayList<Double> coords = new ArrayList<Double>();
            switch (path.get(i)) {
                case "n1":
                    coords.add(-33.935405);
                    coords.add(151.16643);
                    printPath.add(coords);
                    break;
                case "n2":
                    coords.add(-33.935696);
                    coords.add(151.166259);
                    printPath.add(coords);
                    break;
                case "n3":
                    coords.add(-33.935895);
                    coords.add(151.166154);
                    printPath.add(coords);
                    break;
                case "n4":
                    coords.add(-33.936078);
                    coords.add(151.166061);
                    printPath.add(coords);
                    break;
                case "n5":
                    coords.add(-33.93614);
                    coords.add(151.16603);
                    printPath.add(coords);
                    break;
                case "n6":
                    coords.add(-33.936404);
                    coords.add(151.16591);
                    printPath.add(coords);
                    break;
                case "n7":
                    coords.add(-33.936646);
                    coords.add(151.165788);
                    printPath.add(coords);
                    break;
                case "n8":
                    coords.add(-33.936871);
                    coords.add(151.165668);
                    printPath.add(coords);
                    break;
                case "n9":
                    coords.add(-33.937265);
                    coords.add(151.165436);
                    printPath.add(coords);
                    break;
                case "n10":
                    coords.add(-33.937403);
                    coords.add(151.165188);
                    printPath.add(coords);
                    break;
                case "n11":
                    coords.add(-33.937523);
                    coords.add(151.16496);
                    printPath.add(coords);
                    break;
                case "n12":
                    coords.add(-33.937247);
                    coords.add(151.168735);
                    printPath.add(coords);
                    break;
                case "n13":
                    coords.add(-33.936529);
                    coords.add(151.168207);
                    printPath.add(coords);
                    break;
                case "n14":
                    coords.add(-33.938909);
                    coords.add(151.16547);
                    printPath.add(coords);
                    break;
                case "n15":
                    coords.add(-33.9364);
                    coords.add(151.167881);
                    printPath.add(coords);
                    break;
                case "n16":
                    coords.add(-33.937181);
                    coords.add(151.166203);
                    printPath.add(coords);
                    break;
                case "n17":
                    coords.add(-33.939397);
                    coords.add(151.165595);
                    printPath.add(coords);
                    break;
                case "n18":
                    coords.add(-33.936699);
                    coords.add(151.168427);
                    printPath.add(coords);
                    break;
                case "n19":
                    coords.add(-33.9373194);
                    coords.add(151.1663006);
                    printPath.add(coords);
                    break;
                case "n20":
                    coords.add(-33.93677);
                    coords.add(151.168564);
                    printPath.add(coords);
                    break;
                case "n21":
                    coords.add(-33.938968);
                    coords.add(151.165387);
                    printPath.add(coords);
                    break;
                case "n22":
                    coords.add(-33.937682);
                    coords.add(151.165872);
                    printPath.add(coords);
                    break;
                case "n23":
                    coords.add(-33.937647);
                    coords.add(151.166007);
                    printPath.add(coords);
                    break;
                case "n24":
                    coords.add(-33.9379978);
                    coords.add(151.1656935);
                    printPath.add(coords);
                    break;
                case "n25":
                    coords.add(-33.937135);
                    coords.add(151.166102);
                    printPath.add(coords);
                    break;
                case "n26":
                    coords.add(-33.937791);
                    coords.add(151.16578);
                    printPath.add(coords);
                    break;
                case "n27":
                    coords.add(-33.937096);
                    coords.add(151.16638);
                    printPath.add(coords);
                    break;
                case "n29":
                    coords.add(-33.936955);
                    coords.add(151.166352);
                    printPath.add(coords);
                    break;
                case "n30":
                    coords.add(-33.939311);
                    coords.add(151.165321);
                    printPath.add(coords);
                    break;
                case "n31":
                    coords.add(-33.937169);
                    coords.add(151.166386);
                    printPath.add(coords);
                    break;
                case "n32":
                    coords.add(-33.9371391);
                    coords.add(151.1687733);
                    printPath.add(coords);
                    break;
                case "n33":
                    coords.add(-33.937255);
                    coords.add(151.166008);
                    printPath.add(coords);
                    break;
                case "n34":
                    coords.add(-33.937271);
                    coords.add(151.168826);
                    printPath.add(coords);
                    break;
                case "n35":
                    coords.add(-33.936625);
                    coords.add(151.168291);
                    printPath.add(coords);
                    break;
                case "n36":
                    coords.add(-33.93713);
                    coords.add(151.166223);
                    printPath.add(coords);
                    break;
                case "n37":
                    coords.add(-33.937337);
                    coords.add(151.166185);
                    printPath.add(coords);
                    break;
                case "n38":
                    coords.add(-33.937774);
                    coords.add(151.168882);
                    printPath.add(coords);
                    break;
                case "n39":
                    coords.add(-33.939458);
                    coords.add(151.165151);
                    printPath.add(coords);
                    break;
                case "n40":
                    coords.add(-33.938269);
                    coords.add(151.165457);
                    printPath.add(coords);
                    break;
                case "n41":
                    coords.add(-33.938279);
                    coords.add(151.165727);
                    printPath.add(coords);
                    break;
                case "n42":
                    coords.add(-33.937293);
                    coords.add(151.165218);
                    printPath.add(coords);
                    break;
                case "n43":
                    coords.add(-33.937093);
                    coords.add(151.168717);
                    printPath.add(coords);
                    break;
                case "n44":
                    coords.add(-33.937139);
                    coords.add(151.166673);
                    printPath.add(coords);
                    break;
                case "n45":
                    coords.add(-33.937896);
                    coords.add(151.165514);
                    printPath.add(coords);
                    break;
                case "n400":
                    coords.add(-33.936788);
                    coords.add(151.167383);
                    printPath.add(coords);
                    break;
                case "n47":
                    coords.add(-33.937161);
                    coords.add(151.167394);
                    printPath.add(coords);
                    break;
                case "n48":
                    coords.add(-33.937163);
                    coords.add(151.16715);
                    printPath.add(coords);
                    break;
                case "n49":
                    coords.add(-33.937118);
                    coords.add(151.167907);
                    printPath.add(coords);
                    break;
                case "n52":
                    coords.add(-33.937149);
                    coords.add(151.165951);
                    printPath.add(coords);
                    break;
                case "n54":
                    coords.add(-33.936308);
                    coords.add(151.167818);
                    printPath.add(coords);
                    break;
                case "n56":
                    coords.add(-33.937079);
                    coords.add(151.165994);
                    printPath.add(coords);
                    break;
                case "n58":
                    coords.add(-33.936807);
                    coords.add(151.168234);
                    printPath.add(coords);
                    break;
                case "n59":
                    coords.add(-33.937528);
                    coords.add(151.165887);
                    printPath.add(coords);
                    break;
                case "n61":
                    coords.add(-33.939332);
                    coords.add(151.165999);
                    printPath.add(coords);
                    break;
                case "n62":
                    coords.add(-33.93652);
                    coords.add(151.166515);
                    printPath.add(coords);
                    break;
                case "n63":
                    coords.add(-33.939332);
                    coords.add(151.165999);
                    printPath.add(coords);
                    break;
                case "n64":
                    coords.add(-33.939254);
                    coords.add(151.165881);
                    printPath.add(coords);
                    break;
                case "n65":
                    coords.add(-33.93702);
                    coords.add(151.167119);
                    printPath.add(coords);
                    break;
                case "n66":
                    coords.add(-33.937194);
                    coords.add(151.168688);
                    printPath.add(coords);
                    break;
                case "n67":
                    coords.add(-33.937159);
                    coords.add(151.167495);
                    printPath.add(coords);
                    break;
                case "n71":
                    coords.add(-33.937159);
                    coords.add(151.167606);
                    printPath.add(coords);
                    break;
                case "n72":
                    coords.add(-33.937075);
                    coords.add(151.168018);
                    printPath.add(coords);
                    break;
                case "n73":
                    coords.add(-33.936826);
                    coords.add(151.166427);
                    printPath.add(coords);
                    break;
                case "n75":
                    coords.add(-33.937007);
                    coords.add(151.168063);
                    printPath.add(coords);
                    break;
                case "n77":
                    coords.add(-33.937159);
                    coords.add(151.167229);
                    printPath.add(coords);
                    break;
                case "n79":
                    coords.add(-33.937216);
                    coords.add(151.167027);
                    printPath.add(coords);
                    break;
                case "n80":
                    coords.add(-33.937139);
                    coords.add(151.166685);
                    printPath.add(coords);
                    break;
                case "n81":
                    coords.add(-33.936984);
                    coords.add(151.168151);
                    printPath.add(coords);
                    break;
                case "n83":
                    coords.add(-33.937148);
                    coords.add(151.167059);
                    printPath.add(coords);
                    break;
                case "n85":
                    coords.add(-33.937296);
                    coords.add(151.166016);
                    printPath.add(coords);
                    break;
                case "n87":
                    coords.add(-33.936889);
                    coords.add(151.166196);
                    printPath.add(coords);
                    break;
                case "n89":
                    coords.add(-33.936275);
                    coords.add(151.165972);
                    printPath.add(coords);
                    break;
                case "n90":
                    coords.add(-33.937069);
                    coords.add(151.165578);
                    printPath.add(coords);
                    break;
                case "n91":
                    coords.add(-33.936858);
                    coords.add(151.168818);
                    printPath.add(coords);
                    break;
                case "n92":
                    coords.add(-33.936827);
                    coords.add(151.166216);
                    printPath.add(coords);
                    break;
                case "n94":
                    coords.add(-33.936945);
                    coords.add(151.16614);
                    printPath.add(coords);
                    break;
                case "n96":
                    coords.add(-33.936995);
                    coords.add(151.168337);
                    printPath.add(coords);
                    break;
                case "n97":
                    coords.add(-33.938969);
                    coords.add(151.165741);
                    printPath.add(coords);
                    break;
                case "n98":
                    coords.add(-33.937144);
                    coords.add(151.167741);
                    printPath.add(coords);
                    break;
                case "n100":
                    coords.add(-33.936932);
                    coords.add(151.168211);
                    printPath.add(coords);
                    break;
                case "n102":
                    coords.add(-33.937334);
                    coords.add(151.166571);
                    printPath.add(coords);
                    break;
                case "n103":
                    coords.add(-33.937265);
                    coords.add(151.165821);
                    printPath.add(coords);
                    break;
                case "n106":
                    coords.add(-33.938011);
                    coords.add(151.16895);
                    printPath.add(coords);
                    break;
                case "n107":
                    coords.add(-33.93957);
                    coords.add(151.165375);
                    printPath.add(coords);
                    break;
                case "n108":
                    coords.add(-33.939514);
                    coords.add(151.165642);
                    printPath.add(coords);
                    break;
                case "n109":
                    coords.add(-33.9384044);
                    coords.add(151.1656692);
                    printPath.add(coords);
                    break;
                case "n110":
                    coords.add(-33.938033);
                    coords.add(151.165207);
                    printPath.add(coords);
                    break;
                case "n111":
                    coords.add(-33.937006);
                    coords.add(151.1666);
                    printPath.add(coords);
                    break;
                case "n112":
                    coords.add(-33.937067);
                    coords.add(151.166817);
                    printPath.add(coords);
                    break;
                case "n113":
                    coords.add(-33.935637);
                    coords.add(151.166748);
                    printPath.add(coords);
                    break;
                case "n114":
                    coords.add(-33.936192);
                    coords.add(151.167624);
                    printPath.add(coords);
                    break;
                case "n115":
                    coords.add(-33.937135);
                    coords.add(151.1682);
                    printPath.add(coords);
                    break;
                case "n116":
                    coords.add(-33.9368747);
                    coords.add(151.1688705);
                    printPath.add(coords);
                    break;
                case "n117":
                    coords.add(-33.936856);
                    coords.add(151.169325);
                    printPath.add(coords);
                    break;
                case "n118":
                    coords.add(-33.937471);
                    coords.add(151.168768);
                    printPath.add(coords);
                    break;
                case "n119":
                    coords.add(-33.938293);
                    coords.add(151.168894);
                    printPath.add(coords);
                    break;
                case "n140":
                    coords.add(-33.937037);
                    coords.add(151.166924);
                    printPath.add(coords);
                    break;
                case "n141":
                    coords.add(-33.937657);
                    coords.add(151.164883);
                    printPath.add(coords);
                    break;
                case "n143":
                    coords.add(-33.937864);
                    coords.add(151.165357);
                    printPath.add(coords);
                    break;
                case "n145":
                    coords.add(-33.937118);
                    coords.add(151.165552);
                    printPath.add(coords);
                    break;
                case "n146":
                    coords.add(-33.937228);
                    coords.add(151.165625);
                    printPath.add(coords);
                    break;
                case "n147":
                    coords.add(-33.937516);
                    coords.add(151.166179);
                    printPath.add(coords);
                    break;
                case "n149":
                    coords.add(-33.936809);
                    coords.add(151.166275);
                    printPath.add(coords);
                    break;
                case "n153":
                    coords.add(-33.937452);
                    coords.add(151.168729);
                    printPath.add(coords);
                    break;
                case "n157":
                    coords.add(-33.936788);
                    coords.add(151.169225);
                    printPath.add(coords);
                    break;
                case "n158":
                    coords.add(-33.937959);
                    coords.add(151.165121);
                    printPath.add(coords);
                    break;
                case "n161":
                    coords.add(-33.939334);
                    coords.add(151.16546);
                    printPath.add(coords);
                    break;
                case "n162":
                    coords.add(-33.939375);
                    coords.add(151.165428);
                    printPath.add(coords);
                    break;
                case "n163":
                    coords.add(-33.938178);
                    coords.add(151.16568);
                    printPath.add(coords);
                    break;
                case "n164":
                    coords.add(-33.9367325);
                    coords.add(151.1680192);
                    printPath.add(coords);
                    break;
                case "n166":
                    coords.add(-33.936835);
                    coords.add(151.169027);
                    printPath.add(coords);
                    break;
                case "n167":
                    coords.add(-33.93681);
                    coords.add(151.169165);
                    printPath.add(coords);
                    break;
                case "n177":
                    coords.add(-33.936035);
                    coords.add(151.166055);
                    printPath.add(coords);
                    break;
                case "n178":
                    coords.add(-33.937373);
                    coords.add(151.16606);
                    printPath.add(coords);
                    break;
                case "n179":
                    coords.add(-33.936481);
                    coords.add(151.167263);
                    printPath.add(coords);
                    break;
                case "n180":
                    coords.add(-33.9371);
                    coords.add(151.168569);
                    printPath.add(coords);
                    break;
                case "n181":
                    coords.add(-33.938969);
                    coords.add(151.169169);
                    printPath.add(coords);
                    break;
                case "n183":
                    coords.add(-33.938787);
                    coords.add(151.169097);
                    printPath.add(coords);
                    break;
                case "n184":
                    coords.add(-33.938532);
                    coords.add(151.169011);
                    printPath.add(coords);
                    break;
                case "n185":
                    coords.add(-33.938369);
                    coords.add(151.168956);
                    printPath.add(coords);
                    break;
                case "n186":
                    coords.add(-33.937816);
                    coords.add(151.168779);
                    printPath.add(coords);
                    break;
                case "n187":
                    coords.add(-33.937611);
                    coords.add(151.168722);
                    printPath.add(coords);
                    break;
                case "n188":
                    coords.add(-33.937231);
                    coords.add(151.168602);
                    printPath.add(coords);
                    break;
                case "n189":
                    coords.add(-33.936765);
                    coords.add(151.169389);
                    printPath.add(coords);
                    break;
                case "n190":
                    coords.add(-33.9368019);
                    coords.add(151.16944);
                    printPath.add(coords);
                    break;
                case "n191":
                    coords.add(-33.9363505);
                    coords.add(151.1679575);
                    printPath.add(coords);
                    break;
                case "n192":
                    coords.add(-33.9361375);
                    coords.add(151.1678567);
                    printPath.add(coords);
                    break;
                case "n194":
                    coords.add(-33.9384347);
                    coords.add(151.1655362);
                    printPath.add(coords);
                    break;
                case "n195":
                    coords.add(-33.9384716);
                    coords.add(151.1656544);
                    printPath.add(coords);
                    break;
                case "n196":
                    coords.add(-33.9391715);
                    coords.add(151.1658601);
                    printPath.add(coords);
                    break;
                case "n197":
                    coords.add(-33.9386916);
                    coords.add(151.165711);
                    printPath.add(coords);
                    break;
                case "n198":
                    coords.add(-33.9394054);
                    coords.add(151.1659222);
                    printPath.add(coords);
                    break;
                case "n199":
                    coords.add(-33.9395217);
                    coords.add(151.1649883);
                    printPath.add(coords);
                    break;
                case "n202":
                    coords.add(-33.9396574);
                    coords.add(151.1651834);
                    printPath.add(coords);
                    break;
                case "n207":
                    coords.add(-33.93534);
                    coords.add(151.1663585);
                    printPath.add(coords);
                    break;
                case "n208":
                    coords.add(-33.9360072);
                    coords.add(151.1660279);
                    printPath.add(coords);
                    break;
                case "n209":
                    coords.add(-33.9362499);
                    coords.add(151.1659082);
                    printPath.add(coords);
                    break;
                case "n210":
                    coords.add(-33.9365043);
                    coords.add(151.1657834);
                    printPath.add(coords);
                    break;
                case "n211":
                    coords.add(-33.9371986);
                    coords.add(151.1653304);
                    printPath.add(coords);
                    break;
                case "n212":
                    coords.add(-33.937422);
                    coords.add(151.1649276);
                    printPath.add(coords);
                    break;
                case "n213":
                    coords.add(-33.9362651);
                    coords.add(151.1659562);
                    printPath.add(coords);
                    break;
                case "n214":
                    coords.add(-33.9365109);
                    coords.add(151.1658321);
                    printPath.add(coords);
                    break;
                case "n215":
                    coords.add(-33.9354057);
                    coords.add(151.1668056);
                    printPath.add(coords);
                    break;
                case "n216":
                    coords.add(-33.9366471);
                    coords.add(151.1664425);
                    printPath.add(coords);
                    break;
                case "n220":
                    coords.add(-33.9379653);
                    coords.add(151.1651501);
                    printPath.add(coords);
                    break;
                case "n221":
                    coords.add(-33.9360171);
                    coords.add(151.1665364);
                    printPath.add(coords);
                    break;
                case "n222":
                    coords.add(-33.9362312);
                    coords.add(151.1665236);
                    printPath.add(coords);
                    break;
                case "n223":
                    coords.add(-33.9362927);
                    coords.add(151.1665003);
                    printPath.add(coords);
                    break;
                case "n224":
                    coords.add(-33.9366678);
                    coords.add(151.1677894);
                    printPath.add(coords);
                    break;
                case "n225":
                    coords.add(-33.9370465);
                    coords.add(151.1676737);
                    printPath.add(coords);
                    break;
                case "n226":
                    coords.add(-33.936976);
                    coords.add(151.1686204);
                    printPath.add(coords);
                    break;
                case "n227":
                    coords.add(-33.9376792);
                    coords.add(151.1662979);
                    printPath.add(coords);
                    break;
                case "n228":
                    coords.add(-33.9380953);
                    coords.add(151.1655254);
                    printPath.add(coords);
                    break;
                case "n229":
                    coords.add(-33.9365657);
                    coords.add(151.1681453);
                    printPath.add(coords);
                    break;
                case "n230":
                    coords.add(-33.9389527);
                    coords.add(151.1655467);
                    printPath.add(coords);
                    break;
                case "n231":
                    coords.add(-33.936689);
                    coords.add(151.1682352);
                    printPath.add(coords);
                    break;
                case "n232":
                    coords.add(-33.9367634);
                    coords.add(151.1684122);
                    printPath.add(coords);
                    break;
                case "n233":
                    coords.add(-33.9368452);
                    coords.add(151.1685014);
                    printPath.add(coords);
                    break;
                case "n234":
                    coords.add(-33.9368993);
                    coords.add(151.168748);
                    printPath.add(coords);
                    break;
                case "n235":
                    coords.add(-33.9368781);
                    coords.add(151.1688245);
                    printPath.add(coords);
                    break;
                case "n236":
                    coords.add(-33.9374108);
                    coords.add(151.168679);
                    printPath.add(coords);
                    break;
                case "n237":
                    coords.add(-33.9374765);
                    coords.add(151.1686943);
                    printPath.add(coords);
                    break;
                case "n238":
                    coords.add(-33.9377674);
                    coords.add(151.1687759);
                    printPath.add(coords);
                    break;
                case "n239":
                    coords.add(-33.9380309);
                    coords.add(151.1688633);
                    printPath.add(coords);
                    break;
                case "n240":
                    coords.add(-33.9382859);
                    coords.add(151.168942);
                    printPath.add(coords);
                    break;
                case "n241":
                    coords.add(-33.9370546);
                    coords.add(151.167699);
                    printPath.add(coords);
                    break;
                case "n242":
                    coords.add(-33.9370345);
                    coords.add(151.1678619);
                    printPath.add(coords);
                    break;
                case "n243":
                    coords.add(-33.9370028);
                    coords.add(151.16796);
                    printPath.add(coords);
                    break;
                case "n244":
                    coords.add(-33.936954);
                    coords.add(151.1680256);
                    printPath.add(coords);
                    break;
                case "n245":
                    coords.add(-33.9369049);
                    coords.add(151.1680863);
                    printPath.add(coords);
                    break;
                case "n246":
                    coords.add(-33.9369062);
                    coords.add(151.1683037);
                    printPath.add(coords);
                    break;
                case "n247":
                    coords.add(-33.937067);
                    coords.add(151.1675972);
                    printPath.add(coords);
                    break;
                case "n248":
                    coords.add(-33.937072);
                    coords.add(151.1674836);
                    printPath.add(coords);
                    break;
                case "n249":
                    coords.add(-33.937066);
                    coords.add(151.1673838);
                    printPath.add(coords);
                    break;
                case "n250":
                    coords.add(-33.9370472);
                    coords.add(151.1672205);
                    printPath.add(coords);
                    break;
                case "n251":
                    coords.add(-33.9371033);
                    coords.add(151.1669727);
                    printPath.add(coords);
                    break;
                case "n252":
                    coords.add(-33.9371786);
                    coords.add(151.1667767);
                    printPath.add(coords);
                    break;
                case "n253":
                    coords.add(-33.9372922);
                    coords.add(151.1665505);
                    printPath.add(coords);
                    break;
                case "n254":
                    coords.add(-33.9382004);
                    coords.add(151.1655685);
                    printPath.add(coords);
                    break;
                case "n255":
                    coords.add(-33.9383083);
                    coords.add(151.1656013);
                    printPath.add(coords);
                    break;
                case "n256":
                    coords.add(-33.9390048);
                    coords.add(151.1654573);
                    printPath.add(coords);
                    break;
                case "n257":
                    coords.add(-33.9394551);
                    coords.add(151.1652403);
                    printPath.add(coords);
                    break;
                case "n258":
                    coords.add(-33.937057);
                    coords.add(151.1659621);
                    printPath.add(coords);
                    break;
                case "n259":
                    coords.add(-33.9371226);
                    coords.add(151.1658916);
                    printPath.add(coords);
                    break;
                case "n260":
                    coords.add(-33.9368088);
                    coords.add(151.1663281);
                    printPath.add(coords);
                    break;
                case "n261":
                    coords.add(-33.9369158);
                    coords.add(151.1662586);
                    printPath.add(coords);
                    break;
                case "n262":
                    coords.add(-33.9369703);
                    coords.add(151.1662433);
                    printPath.add(coords);
                    break;
                case "n263":
                    coords.add(-33.936859);
                    coords.add(151.1662902);
                    printPath.add(coords);
                    break;
                case "n264":
                    coords.add(-33.9370906);
                    coords.add(151.1662982);
                    printPath.add(coords);
                    break;
                case "n265":
                    coords.add(-33.9372799);
                    coords.add(151.1660762);
                    printPath.add(coords);
                    break;
                case "n266":
                    coords.add(-33.9375531);
                    coords.add(151.1659233);
                    printPath.add(coords);
                    break;
                case "n267":
                    coords.add(-33.9371456);
                    coords.add(151.166298);
                    printPath.add(coords);
                    break;
                case "n268":
                    coords.add(-33.9372388);
                    coords.add(151.1662843);
                    printPath.add(coords);
                    break;
                case "n269":
                    coords.add(-33.9376025);
                    coords.add(151.1658119);
                    printPath.add(coords);
                    break;
                case "n270":
                    coords.add(-33.9376877);
                    coords.add(151.1657078);
                    printPath.add(coords);
                    break;
            }
        }
        return (printPath);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<String> endNodes = getIntent().getStringArrayListExtra("key");
        ArrayList<String> stringLocation = getIntent().getStringArrayListExtra("key2");
        currentLocation[0] = Double.parseDouble(stringLocation.get(0));
        currentLocation[1] = Double.parseDouble(stringLocation.get(1));
        route = new ArrayList<ArrayList<Double>>();

        if (endNodes != null) {

            //set a default current location if none exist
            if(currentLocation[0] == null){
                currentLocation[0] = -33.9366;
                currentLocation[1] = 151.1660;
                ArrayList<String> path = Dijkstra.passLocations(endNodes, currentLocation);
                route = getCoords(path, currentLocation);
            }else{
                ArrayList<String> path = Dijkstra.passLocations(endNodes, currentLocation);
                route = getCoords(path, currentLocation);
            }
        }

        mapsActivityLayoutView = new mapsActivityLayout(this, route);
        setContentView(mapsActivityLayoutView);

    }

    @Override
    public void onPause() {
        super.onPause();
        mapsActivityLayoutView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapsActivityLayoutView.resume();
    }

}
